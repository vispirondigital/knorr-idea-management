# Knorr idea management
---
# knorr-idea-management-android

##1. Used technologies
	- Android Architecture Components
	- Dagger
	- RxJava
	- RoomDb/RealmDb (needs to be discussed)
	- Android Navigation Component
	- ButterKnife
	- AWS libraries
	
##2. Architecture
	MVVM with android architecture components based on the example from Google on GitHub [GitHubBrowserSample](https://github.com/googlesamples/android-architecture-components/tree/master/GithubBrowserSample)

##3. Usage
	To build and run the application version 3.2 of Android Studio is required
