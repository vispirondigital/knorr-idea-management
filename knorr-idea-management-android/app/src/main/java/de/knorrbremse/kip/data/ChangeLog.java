package de.knorrbremse.kip.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ChangeLog implements Parcelable {

    @SerializedName("implementationStatus")
    private Change implementationStatus;

    @SerializedName("status")
    private Change status;

    protected ChangeLog(Parcel in) {
        implementationStatus = in.readParcelable(Change.class.getClassLoader());
        status = in.readParcelable(Change.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(implementationStatus, flags);
        dest.writeParcelable(status, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ChangeLog> CREATOR = new Creator<ChangeLog>() {
        @Override
        public ChangeLog createFromParcel(Parcel in) {
            return new ChangeLog(in);
        }

        @Override
        public ChangeLog[] newArray(int size) {
            return new ChangeLog[size];
        }
    };

    public ChangeLog(Change implementationStatus, Change status) {
        this.implementationStatus = implementationStatus;
        this.status = status;
    }

    public Change getImplementationStatus() {
        return implementationStatus;
    }

    public void setImplementationStatus(Change implementationStatus) {
        this.implementationStatus = implementationStatus;
    }

    public Change getStatus() {
        return status;
    }

    public void setStatus(Change status) {
        this.status = status;
    }
}
