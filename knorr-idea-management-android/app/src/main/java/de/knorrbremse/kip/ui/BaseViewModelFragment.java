package de.knorrbremse.kip.ui;

import android.arch.lifecycle.ViewModelProvider;
import android.os.Bundle;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import de.knorrbremse.kip.MainActivity;
import de.knorrbremse.kip.di.Injectable;

public abstract class BaseViewModelFragment<VM> extends BaseFragment implements Injectable {

    public VM viewModel;

    public MainActivity activity;

    @Inject
    public ViewModelProvider.Factory viewModelFactory;

    public abstract void createViewModel();

    public abstract void subscribe();

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = (MainActivity) getActivity();
        createViewModel();
        subscribe();
    }
}
