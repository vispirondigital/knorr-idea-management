package de.knorrbremse.kip.data.enums;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.StringRes;

import java.util.HashMap;
import java.util.Map;

import de.knorrbremse.kip.R;

public enum WorkArea implements Parcelable {

    LASERLINIE_01(0, R.string.laserilinie_01),
    LASERLINIE_02(1, R.string.laserilinie_02),
    ZELLE_EB_01(2, R.string.zelle_eb_01),
    ZELLE_EB_02(3, R.string.zelle_eb_02),
    ZELLE_EB_06(4, R.string.zelle_eb_06),
    ZELLE_EB_08(5, R.string.zelle_eb_08),
    NUTRO(6, R.string.nutro),
    UV_LACK(7, R.string.uv_lack),
    UP_LINIE(8, R.string.up_linie),
    CAT_LINIE(9, R.string.cat_linie),
    BORDELZELLE(10, R.string.bordelzelle),
    BAZ(11, R.string.baz),
    WASCHANLAGEN(12, R.string.waschanlagen),
    STA(13, R.string.sta),
    SONSTIGES(14, R.string.sonstiges);

    private static Map<Integer, WorkArea> workAreas = new HashMap<>();

    static {
        for (WorkArea area : values()) {
            workAreas.put(area.value, area);
        }
    }

    private int value;
    @StringRes
    int string;

    public int getString() {
        return string;
    }

    public int getValue() {
        return value;
    }

    WorkArea(int value, @StringRes int string) {
        this.value = value;
        this.string = string;
    }

    WorkArea(Parcel in) {
        value = in.readInt();
        string = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(value);
        dest.writeInt(string);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<WorkArea> CREATOR = new Creator<WorkArea>() {
        @Override
        public WorkArea createFromParcel(Parcel in) {
            return WorkArea.fromInt(in.readInt());
        }

        @Override
        public WorkArea[] newArray(int size) {
            return new WorkArea[size];
        }
    };

    public static WorkArea fromInt(int value) {
        return workAreas.get(value);
    }
}
