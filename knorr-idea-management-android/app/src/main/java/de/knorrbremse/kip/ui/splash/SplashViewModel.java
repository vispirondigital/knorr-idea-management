package de.knorrbremse.kip.ui.splash;

import android.content.Context;
import android.content.SharedPreferences;

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;

import javax.inject.Inject;

import de.knorrbremse.kip.custom.KeyHelper;
import de.knorrbremse.kip.custom.SingleLiveEvent;
import de.knorrbremse.kip.remote.TokenProvider;
import de.knorrbremse.kip.db.RoomDbProvider;
import de.knorrbremse.kip.remote.api.IdeaApi;
import de.knorrbremse.kip.viewmodel.BaseViewModel;

public class SplashViewModel extends BaseViewModel implements TokenProvider.TokenRefreshListener {

    @Inject
    public SplashViewModel() {
    }

    @Inject
    CognitoUserPool userPool;

    @Inject
    KeyHelper helper;

    @Inject
    TokenProvider tokenProvider;

    @Inject
    IdeaApi api;

    @Inject
    RoomDbProvider provider;

    @Inject
    Context context;

    public SingleLiveEvent<Boolean> getLoggedIn() {
        return loggedIn;
    }

    private SingleLiveEvent<Boolean> loggedIn = new SingleLiveEvent<>();

    public void checkLoggedIn() {
        CognitoUser user = userPool.getCurrentUser();
        final SharedPreferences prefs = context.getSharedPreferences("CognitoIdentityProviderCache", 0);
        final String csiIdTokenKey = String.format("CognitoIdentityProvider.%s.%s.idToken", userPool.getClientId(), user.getUserId());
        if (prefs.contains(csiIdTokenKey)) {
            tokenProvider.refreshTokenInBackground(this);
        } else
            loggedIn.postValue(false);
    }


    @Override
    public void onSuccess() {
        loggedIn.postValue(true);
    }

    @Override
    public void onFailure(Exception exception) {
        loggedIn.postValue(false);
    }
}
