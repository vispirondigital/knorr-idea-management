package de.knorrbremse.kip.ui.verification;

import android.content.Context;

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoDevice;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ChallengeContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GetDetailsHandler;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.inject.Inject;

import de.knorrbremse.kip.custom.KeyHelper;
import de.knorrbremse.kip.custom.SingleLiveEvent;
import de.knorrbremse.kip.remote.TokenProvider;
import de.knorrbremse.kip.viewmodel.BaseViewModel;
import timber.log.Timber;

public class VerificationViewModel extends BaseViewModel implements AuthenticationHandler {

    private String password;
    private CognitoUser user;

    @Inject
    public VerificationViewModel() {
    }

    @Inject
    CognitoUserPool userPool;

    @Inject
    TokenProvider tokenProvider;

    @Inject
    KeyHelper keyHelper;

    @Inject
    Context context;

    private SingleLiveEvent<String> confirmed = new SingleLiveEvent<>();

    public SingleLiveEvent<String> getConfirmed() {
        return confirmed;
    }

    private SingleLiveEvent<Exception> exception = new SingleLiveEvent<>();

    public SingleLiveEvent<Exception> getException() {
        return exception;
    }

    /**
     * To check if the user has confirmed their email we actually have to try to login and check if the error is for the unconfirmed email address
     *
     * @param id
     * @param password
     */
    public void checkConfirmation(String id, String password) {
        Timber.d("email: " + id + ", password: " + password);
        this.password = password;
        user = userPool.getUser(id);
        user.getSessionInBackground(this);
    }

    @Override
    public void onSuccess(CognitoUserSession userSession, CognitoDevice newDevice) {
        try {
            keyHelper.storePasswordInPrefs(context, password);
            tokenProvider.storeToken(userSession.getIdToken().getJWTToken());
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | NoSuchProviderException | UnsupportedEncodingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        user.getDetailsInBackground(new GetDetailsHandler() {
            @Override
            public void onSuccess(CognitoUserDetails cognitoUserDetails) {
                confirmed.postValue(cognitoUserDetails.getAttributes().getAttributes().get("email"));
            }

            @Override
            public void onFailure(Exception exception) {

            }
        });

    }

    @Override
    public void getAuthenticationDetails(AuthenticationContinuation authenticationContinuation, String userId) {
        AuthenticationDetails authenticationDetails = new AuthenticationDetails(userId, password, null);

        // Pass the user sign-in credentials to the continuation
        authenticationContinuation.setAuthenticationDetails(authenticationDetails);

        // Allow the sign-in to continue
        authenticationContinuation.continueTask();
    }

    @Override
    public void getMFACode(MultiFactorAuthenticationContinuation continuation) {

    }

    @Override
    public void authenticationChallenge(ChallengeContinuation continuation) {

    }

    @Override
    public void onFailure(Exception e) {
        Timber.e(e);
        exception.postValue(e);
    }
}
