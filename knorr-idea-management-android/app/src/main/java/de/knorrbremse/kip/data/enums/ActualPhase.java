package de.knorrbremse.kip.data.enums;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.StringRes;

import java.util.HashMap;
import java.util.Map;

import de.knorrbremse.kip.R;

public enum ActualPhase implements Parcelable {

    IN_TEST(0, R.string.testphase),
    IN_PROGRESS(1, R.string.implemented),
    IN_REVIEW(2, R.string.examination),
    COMMITED(3, R.string.received);

    private static Map<Integer, ActualPhase> phases;
    private int phase;
    @StringRes
    int string;

    static {
        phases = new HashMap<>();
        for (ActualPhase phase : values()) {
            phases.put(phase.phase, phase);
        }
    }

    ActualPhase(int phase, @StringRes int string) {
        this.phase = phase;
        this.string = string;
    }

    ActualPhase(Parcel in) {
        phase = in.readInt();
        string = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(phase);
        dest.writeInt(string);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ActualPhase> CREATOR = new Creator<ActualPhase>() {
        @Override
        public ActualPhase createFromParcel(Parcel in) {
            return ActualPhase.fromInt(in.readInt());
        }

        @Override
        public ActualPhase[] newArray(int size) {
            return new ActualPhase[size];
        }
    };

    public @StringRes
    int getString() {
        return string;
    }

    public static ActualPhase fromInt(int phase) {
        return phases.get(phase);
    }

    public static ActualPhase fromString(String string) {
        switch (string) {
            case "COMMITED":
                return ActualPhase.COMMITED;
            case "IN_TEST":
                return ActualPhase.IN_TEST;
            case "IN_REVIEW":
                return ActualPhase.IN_REVIEW;
            case "IN_PROGRESS":
                return ActualPhase.IN_PROGRESS;
        }
        return null;
    }
}
