package de.knorrbremse.kip.di;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import de.knorrbremse.kip.data.Constants;
import de.knorrbremse.kip.remote.AuthInterceptor;
import de.knorrbremse.kip.remote.TokenProvider;
import de.knorrbremse.kip.remote.api.IdeaApi;
import de.knorrbremse.kip.remote.api.IdeaApiImpl;
import de.knorrbremse.kip.remote.service.IdeasService;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {

    private final String BASE_URL = Constants.OK_HTTP_CLIENT_BASE_URL;

    @Provides
    @ApplicationScope
    public OkHttpClient provideOkHttpClient(AuthInterceptor interceptor) {
        return new OkHttpClient.Builder()
                .writeTimeout(40000, TimeUnit.MILLISECONDS)
                .readTimeout(40000, TimeUnit.MILLISECONDS)
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .addInterceptor(interceptor)
                .build();
    }

    @Provides
    @ApplicationScope
    public Gson provideGson() {
        return new GsonBuilder().setLenient().create();
    }

    @Provides
    @ApplicationScope
    public AuthInterceptor provideAuthInterceptor(TokenProvider provider) {
        return new AuthInterceptor(provider);
    }

    @Provides
    @ApplicationScope
    public Retrofit provideRetrofit(OkHttpClient okHttpClient, Gson gson) {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();
    }

    @Provides
    @ApplicationScope
    public IdeaApi provideIdeaApi(Retrofit retrofit, TokenProvider provider) {
        return new IdeaApiImpl(retrofit.create(IdeasService.class), provider);
    }
}
