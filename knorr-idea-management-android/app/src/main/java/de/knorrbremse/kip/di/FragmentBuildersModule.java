package de.knorrbremse.kip.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import de.knorrbremse.kip.ui.createidea.CreateIdeaFragment;
import de.knorrbremse.kip.ui.group.GroupFragment;
import de.knorrbremse.kip.ui.ideadetail.IdeaDetailFragment;
import de.knorrbremse.kip.ui.ideas.IdeasFragment;
import de.knorrbremse.kip.ui.login.LoginFragment;
import de.knorrbremse.kip.ui.registration.RegistrationFragment;
import de.knorrbremse.kip.ui.splash.SplashFragment;
import de.knorrbremse.kip.ui.verification.VerificationFragment;

@SuppressWarnings("unused")
@Module
public abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract RegistrationFragment contributeRegistrationFragment();

    @ContributesAndroidInjector
    abstract VerificationFragment contributeVerificationFragment();

    @ContributesAndroidInjector
    abstract LoginFragment contributeLoginFragment();

    @ContributesAndroidInjector
    abstract IdeasFragment contributeIdeasFragment();

    @ContributesAndroidInjector
    abstract IdeaDetailFragment contributeIdeaDetailFragment();

    @ContributesAndroidInjector
    abstract SplashFragment contributeSplashFragment();

    @ContributesAndroidInjector
    abstract CreateIdeaFragment contributeCreateIdeaFragment();

    @ContributesAndroidInjector
    abstract GroupFragment contributeGroupFragment();

}
