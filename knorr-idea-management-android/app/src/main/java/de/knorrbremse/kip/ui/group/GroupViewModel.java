package de.knorrbremse.kip.ui.group;

import android.annotation.SuppressLint;
import android.arch.lifecycle.MutableLiveData;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import de.knorrbremse.kip.custom.SingleLiveEvent;
import de.knorrbremse.kip.data.Cocreator;
import de.knorrbremse.kip.db.RoomDbProvider;
import de.knorrbremse.kip.viewmodel.BaseViewModel;

public class GroupViewModel extends BaseViewModel {

    @Inject
    GroupViewModel() {
    }

    @Inject
    RoomDbProvider provider;

    private SingleLiveEvent<Map<Integer, Cocreator>> result = new SingleLiveEvent<>();

    public SingleLiveEvent<Map<Integer, Cocreator>> getResult() {
        return result;
    }

    @SuppressLint("UseSparseArrays")
    public void findUser(String id, int viewId) {
        addDisposable(provider.getCocreator(id).subscribe((cocreator, throwable) -> {
            HashMap<Integer, Cocreator> map = new HashMap<>();
            if (cocreator != null) {
                map.put(viewId, cocreator);
            }
            if (throwable != null) {
                map.put(viewId, null);
            }
            result.postValue(map);
        }));
    }
}
