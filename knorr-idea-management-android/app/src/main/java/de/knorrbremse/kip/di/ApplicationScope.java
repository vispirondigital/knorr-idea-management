package de.knorrbremse.kip.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;
import javax.inject.Singleton;

@Scope
@Retention(RetentionPolicy.CLASS)
public @interface ApplicationScope {
}
