package de.knorrbremse.kip.data.enums;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.StringRes;

import java.util.HashMap;
import java.util.Map;

import de.knorrbremse.kip.R;

public enum Category implements Parcelable {

    WORKPLACE_SECURITY(0, R.string.workplace_security),
    QUALITY(1, R.string.quality),
    PRODUCTIVITY(2, R.string.productivity);

    private static Map<Integer, Category> categories;

    private int value;

    public int getString() {
        return string;
    }
    public int getValue() {
        return value;
    }

    @StringRes
    int string;

    static {
        categories = new HashMap<>();
        for (Category category : values()) {
            categories.put(category.value, category);
        }
    }

    Category(int value, @StringRes int string) {
        this.value = value;
        this.string = string;
    }

    public static Category fromInt(int value) {
        return categories.get(value);
    }

    Category(Parcel in) {
        value = in.readInt();
        string = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(value);
        dest.writeInt(string);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return Category.fromInt(in.readInt());
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
}
