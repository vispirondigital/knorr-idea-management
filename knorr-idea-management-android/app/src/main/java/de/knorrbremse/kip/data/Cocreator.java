package de.knorrbremse.kip.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "employees")
public class Cocreator implements Parcelable {

    @PrimaryKey()
    @ColumnInfo(name = "personalId")
    @SerializedName("personalId")
    @NonNull
    String personalId;


    @ColumnInfo(name = "email")
    @SerializedName("email")
    String email;

    @ColumnInfo(name = "firstName")
    @SerializedName("firstName")
    String firstName;

    @ColumnInfo(name = "lastName")
    @SerializedName("lastName")
    String lastName;


    protected Cocreator(Parcel in) {
        personalId = in.readString();
        email = in.readString();
        firstName = in.readString();
        lastName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(personalId);
        dest.writeString(email);
        dest.writeString(firstName);
        dest.writeString(lastName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Cocreator> CREATOR = new Creator<Cocreator>() {
        @Override
        public Cocreator createFromParcel(Parcel in) {
            return new Cocreator(in);
        }

        @Override
        public Cocreator[] newArray(int size) {
            return new Cocreator[size];
        }
    };

    public Cocreator(String personalId, String email, String firstName, String lastName) {
        this.personalId = personalId;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public static Cocreator createEmpty() {
        return new Cocreator("", "", "", "");
    }

    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        Cocreator cocreator = (Cocreator) obj;
        if (this.personalId.equalsIgnoreCase(cocreator.personalId))
            return true;
        else return false;
    }
}
