package de.knorrbremse.kip.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Log implements Parcelable {

    @SerializedName("changedAt")
    private
    long createdAt;

    @SerializedName("changedBy")
    private Cocreator changedBy;

    @SerializedName("changelog")
    private ChangeLog changeLog;

    protected Log(Parcel in) {
        createdAt = in.readLong();
        changedBy = in.readParcelable(Cocreator.class.getClassLoader());
        changeLog = in.readParcelable(ChangeLog.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(createdAt);
        dest.writeParcelable(changedBy, flags);
        dest.writeParcelable(changeLog, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Log> CREATOR = new Creator<Log>() {
        @Override
        public Log createFromParcel(Parcel in) {
            return new Log(in);
        }

        @Override
        public Log[] newArray(int size) {
            return new Log[size];
        }
    };

    public Log(long createdAt, Cocreator changedBy, ChangeLog changeLog) {
        this.createdAt = createdAt;
        this.changedBy = changedBy;
        this.changeLog = changeLog;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public Cocreator getChangedBy() {
        return changedBy;
    }

    public void setChangedBy(Cocreator changedBy) {
        this.changedBy = changedBy;
    }

    public ChangeLog getChangeLog() {
        return changeLog;
    }

    public void setChangeLog(ChangeLog changeLog) {
        this.changeLog = changeLog;
    }
}
