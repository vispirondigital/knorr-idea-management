package de.knorrbremse.kip.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

public class Idea implements Parcelable {

    private @NonNull
    String ideaName;
    private @NonNull
    String date;
    private @NonNull
    String description;
    private boolean group = false;
    private boolean accepted = false;
    private int reward;


    protected Idea(Parcel in) {
        ideaName = in.readString();
        date = in.readString();
        description = in.readString();
        group = in.readByte() != 0;
        accepted = in.readByte() != 0;
        reward = in.readInt();
    }

    public static final Creator<Idea> CREATOR = new Creator<Idea>() {
        @Override
        public Idea createFromParcel(Parcel in) {
            return new Idea(in);
        }

        @Override
        public Idea[] newArray(int size) {
            return new Idea[size];
        }
    };

    @NonNull
    public String getIdeaName() {
        return ideaName;
    }

    @NonNull
    public String getDate() {
        return date;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    public boolean isGroup() {
        return group;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public int getReward() {
        return reward;
    }

    public void setIdeaName(@NonNull String ideaName) {
        this.ideaName = ideaName;
    }

    public void setDate(@NonNull String date) {
        this.date = date;
    }

    public void setDescription(@NonNull String description) {
        this.description = description;
    }

    public void setGroup(boolean group) {
        this.group = group;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public void setReward(int reward) {
        this.reward = reward;
    }

    public Idea(String ideaName, String date, String description, boolean group, boolean accepted, int reward) {
        this.ideaName = ideaName;
        this.date = date;
        this.description = description;
        this.group = group;
        this.accepted = accepted;
        this.reward = reward;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(ideaName);
        parcel.writeString(date);
        parcel.writeString(description);
        parcel.writeByte((byte) (group ? 1 : 0));
        parcel.writeByte((byte) (accepted ? 1 : 0));
        parcel.writeInt(reward);
    }
}
