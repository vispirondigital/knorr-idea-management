package de.knorrbremse.kip;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import de.knorrbremse.kip.data.Constants;
import de.knorrbremse.kip.ui.ideas.IdeasFragment;
import io.reactivex.disposables.Disposable;

/**
 * MainActivity used only as a container with the usage of new Navigation component
 */
public class MainActivity extends AppCompatActivity implements HasSupportFragmentInjector {

    @BindView(R.id.rl_toolbar)
    RelativeLayout rlToolbar;

    @BindView(R.id.ll_logout)
    LinearLayout llLogout;

    @BindView(R.id.ib_back)
    ImageButton ibBack;

    @OnClick(R.id.ib_back)
    public void back() {
        onBackPressed();
    }

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.ll_reward)
    LinearLayout llReward;

    @BindView(R.id.tv_reward)
    TextView tvReward;

    @BindView(R.id.ll_loading)
    LinearLayout llLoading;

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

    public void setupToolbar(String title, boolean showBack, boolean showLogout) {
        tvTitle.setText(title);
        ibBack.setVisibility(showBack ? View.VISIBLE : View.GONE);
        llLogout.setVisibility(showLogout ? View.VISIBLE : View.GONE);
    }

    public void showToolbar(boolean show) {
        rlToolbar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void showReward(int amount) {
        if (amount >= Constants.BONUS_LOW && amount < Constants.BONUS_MID) {
            llReward.setBackgroundResource(R.drawable.circle_gold_bcg);
        } else {
            llReward.setBackgroundResource(R.drawable.circle_green_bcg);
        }
        if (amount < Constants.HIGHEST_BONUS) {
            tvReward.setText(getString(R.string.reward_text, String.valueOf(amount)));
        } else {
            tvReward.setText(getString(R.string.over_fifty));
        }
        llReward.setVisibility(View.VISIBLE);

    }

    public void showLoading(boolean loading) {
        llLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    public void hideReward() {
        llReward.setVisibility(View.GONE);
    }

    public LinearLayout getLogout() {
        return llLogout;
    }
}
