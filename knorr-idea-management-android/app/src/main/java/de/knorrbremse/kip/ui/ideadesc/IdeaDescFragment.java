package de.knorrbremse.kip.ui.ideadesc;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.EditText;

import androidx.navigation.Navigation;
import butterknife.BindView;
import butterknife.OnClick;
import de.knorrbremse.kip.R;
import de.knorrbremse.kip.data.CreateIdeaRequest;
import de.knorrbremse.kip.data.Idea;
import de.knorrbremse.kip.ui.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class IdeaDescFragment extends BaseFragment {

    @BindView(R.id.til_idea_desc)
    TextInputLayout tilIdeaDesc;

    @BindView(R.id.et_idea_desc)
    EditText etIdeaDesc;

    @OnClick(R.id.ib_save_desc)
    public void saveIdea() {
        idea.setDescription(etIdeaDesc.getText().toString());
        Bundle bundle = new Bundle();
        bundle.putParcelable("idea", idea);
        Navigation.findNavController(getView()).navigate(R.id.action_ideaDescFragment_to_createIdeaFragment, bundle);
    }

    public IdeaDescFragment() {
        // Required empty public constructor
    }

    private CreateIdeaRequest idea;


    @Override
    public int getLayoutId() {
        return R.layout.fragment_idea_desc;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        idea = (CreateIdeaRequest) getArguments().get("idea");
        etIdeaDesc.setText(idea.getDescription());
    }
}
