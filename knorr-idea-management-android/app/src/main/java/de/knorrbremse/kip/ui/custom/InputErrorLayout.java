package de.knorrbremse.kip.ui.custom;

import android.animation.LayoutTransition;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.StringRes;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.knorrbremse.kip.R;
import timber.log.Timber;


/**
 * Custom view for showing the error for textfield exactly as it is in the design document.
 */
public class InputErrorLayout extends RelativeLayout {

    @BindView(R.id.parent)
    RelativeLayout rlParent;

    @BindView(R.id.til)
    TextInputLayout til;

    @BindView(R.id.et)
    TextInputEditText et;

    @BindView(R.id.iv_error)
    ImageView ivError;

    @BindView(R.id.ib_show_password)
    ImageButton ibShowPassword;

    @BindView(R.id.ll_delete_member)
    LinearLayout llDeleteMember;

    @OnClick(R.id.ll_delete_member)
    public void deleteClicked() {
        if (listener != null)
            listener.onDeleteClicked(this);
    }

    @OnClick(R.id.ib_show_password)
    public void showPassword() {
        if (!showPassword) {
            et.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            ibShowPassword.setImageResource(R.drawable.ic_visibility_off_blue_22dp);
            showPassword = true;
        } else {
            et.setTransformationMethod(PasswordTransformationMethod.getInstance());
            ibShowPassword.setImageResource(R.drawable.ic_visibility_blue_22dp);
            showPassword = false;
        }
    }

    @BindView(R.id.tv_error)
    TextView tvError;

    @BindView(R.id.tv_employee_name)
    TextView tvEmployeeName;

    @BindView(R.id.v_bottom_line)
    View vBottomLine;

    private String error;
    private String hint;
    private int inputType;
    private Drawable drawable;
    private int maxLines;
    private int maxLength;
    private TextWatcher textWatcher;
    private boolean showPassword = false;
    private boolean hasError = false;
    private DeleteListener listener;
    private int imeAction;

    /**
     * Colour fields for the TextInputLayout hint colour settings
     */
    private int[][] states = new int[][]{
            new int[]{android.R.attr.state_enabled}, // enabled
            new int[]{-android.R.attr.state_enabled}, // disabled
            new int[]{-android.R.attr.state_checked}, // unchecked
            new int[]{android.R.attr.state_pressed}  // pressed
    };

    private int[] colorsError = new int[]{
            getResources().getColor(R.color.red),
            getResources().getColor(R.color.red),
            getResources().getColor(R.color.red),
            getResources().getColor(R.color.red),
    };

    private int[] colors = new int[]{
            getResources().getColor(R.color.colorAccent),
            getResources().getColor(R.color.black),
            getResources().getColor(R.color.black),
            getResources().getColor(R.color.colorAccent),
    };

    public TextInputEditText getEt() {
        return et;
    }

    public void setTextWatcher(TextWatcher textWatcher) {
        this.textWatcher = textWatcher;
        et.addTextChangedListener(textWatcher);
    }

    public void removeTextWatcher() {
        et.removeTextChangedListener(textWatcher);
        textWatcher = null;
    }

    public InputErrorLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.input_error_layout, this, true);
        ButterKnife.bind(this, v);
        getAttrs(context, attrs);
        init();
    }

    public InputErrorLayout(Context context) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.input_error_layout, this, true);
        ButterKnife.bind(this, v);
    }

    private void getAttrs(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs, R.styleable.InputErrorLayout, 0, 0
        );
        try {
            error = a.getString(R.styleable.InputErrorLayout_error);
            hint = a.getString(R.styleable.InputErrorLayout_hint);
            inputType = a.getInt(R.styleable.InputErrorLayout_inputType, 3);
            drawable = a.getDrawable(R.styleable.InputErrorLayout_drawable);
            maxLines = a.getInt(R.styleable.InputErrorLayout_maxLines, 100);
            maxLength = a.getInt(R.styleable.InputErrorLayout_maxLength, 10000);
            imeAction = a.getInt(R.styleable.InputErrorLayout_imeAction, 0);
        } catch (Exception e) {
            Timber.e(e);
        } finally {
            a.recycle();
        }
    }

    @SuppressLint("CheckResult")
    public void init() {
        LayoutTransition trans = rlParent.getLayoutTransition();
        trans.enableTransitionType(LayoutTransition.CHANGING);
        trans = llDeleteMember.getLayoutTransition();
        trans.setStartDelay(LayoutTransition.CHANGING, 0);
        et.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
        til.setHint(hint);
        tvError.setText(error);
        et.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
        switch (inputType) {
            case 0:
                et.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                break;
            case 1:
                et.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                et.setTransformationMethod(PasswordTransformationMethod.getInstance());
                ibShowPassword.setVisibility(VISIBLE);
                break;
            case 2:
                et.setInputType(InputType.TYPE_CLASS_NUMBER);
                break;
            case 3:
                et.setInputType(InputType.TYPE_CLASS_TEXT);
        }
        switch (imeAction) {
            case 0:
                et.setImeOptions(EditorInfo.IME_ACTION_NEXT);
                break;
            case 1:
                et.setImeOptions(EditorInfo.IME_ACTION_DONE);
        }
        et.setMaxLines(maxLines);
    }

    public void showError() {
        tvEmployeeName.setVisibility(GONE);
        tvError.setVisibility(VISIBLE);
        ivError.setVisibility(VISIBLE);
        til.setDefaultHintTextColor(new ColorStateList(states, colorsError));
        vBottomLine.setBackgroundResource(R.color.red);
        hasError = true;
    }

    public void hideError() {
        tvError.setVisibility(INVISIBLE);
        ivError.setVisibility(INVISIBLE);
        til.setDefaultHintTextColor(new ColorStateList(states, colors));
        vBottomLine.setBackgroundResource(R.color.black_22);
        hasError = false;
    }

    public void prefill(String text) {
        et.setText(text);
        et.setSelection(0);
    }

    public static InputErrorLayout createEmptyForGroup(Context context) {
        InputErrorLayout inputErrorLayout = new InputErrorLayout(context);
        inputErrorLayout.setDrawable(context.getResources().getDrawable(R.drawable.ic_person_grey_24dp));
        inputErrorLayout.setInputType(2);
        inputErrorLayout.setHint(context.getString(R.string.personal_number));
        inputErrorLayout.setMaxLines(1);
        inputErrorLayout.setMaxLength(20);
        inputErrorLayout.setError(context.getString(R.string.number_not_in_database));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.width = RelativeLayout.LayoutParams.MATCH_PARENT;
        params.height = RelativeLayout.LayoutParams.WRAP_CONTENT;
        inputErrorLayout.setLayoutParams(params);
        inputErrorLayout.init();
        return inputErrorLayout;
    }

    public void showClear(boolean show) {
        llDeleteMember.setVisibility(show ? VISIBLE : GONE);
    }

    public void setEmployeeName(String name) {
        tvEmployeeName.setText(name);
        tvEmployeeName.setVisibility(VISIBLE);
    }

    public String getText() {
        return et.getText().toString();
    }

    public boolean hasError() {
        return hasError;
    }

    public void setError(@StringRes int error) {
        tvError.setText(error);
    }

    public void setError(String error) {
        this.error = error;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public void setInputType(int inputType) {
        this.inputType = inputType;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    public void setMaxLines(int maxLines) {
        this.maxLines = maxLines;
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }

    public void setListener(DeleteListener listener) {
        this.listener = listener;
    }

    public interface DeleteListener {
        void onDeleteClicked(View view);
    }
}
