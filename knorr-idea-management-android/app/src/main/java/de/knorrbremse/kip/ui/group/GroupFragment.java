package de.knorrbremse.kip.ui.group;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import androidx.navigation.Navigation;
import butterknife.BindView;
import butterknife.OnClick;
import de.knorrbremse.kip.MainActivity;
import de.knorrbremse.kip.R;
import de.knorrbremse.kip.data.Cocreator;
import de.knorrbremse.kip.data.CreateIdeaRequest;
import de.knorrbremse.kip.data.enums.Category;
import de.knorrbremse.kip.data.enums.IdeaType;
import de.knorrbremse.kip.data.enums.WorkArea;
import de.knorrbremse.kip.ui.BaseViewModelFragment;
import de.knorrbremse.kip.ui.custom.InputErrorLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupFragment extends BaseViewModelFragment<GroupViewModel> {

    @BindView(R.id.ll_members)
    LinearLayout llMembers;

    @OnClick(R.id.ib_add_member)
    public void addMember() {
        addEmptyMember();
        request.getCocreators().add(Cocreator.createEmpty());
    }

    @OnClick(R.id.btn_clear)
    public void clearMembers() {
        llMembers.removeViews(0, llMembers.getChildCount() - 1);
        request.getCocreators().clear();
        addEmptyMember();
        request.getCocreators().add(Cocreator.createEmpty());
    }

    @OnClick(R.id.btn_save)
    public void saveGroup() {
        boolean proceed = true;
        for (int i = 0; i < llMembers.getChildCount() - 1; i++) {
            InputErrorLayout iel = (InputErrorLayout) llMembers.getChildAt(i);
            if (iel.hasError()) {
                proceed = false;
            }
        }
        if (proceed) {
            if (!checkIfAllEmpty()) {
                request.setIdeaType(IdeaType.GROUP);
            }
            filterSame();
            Bundle bundle = new Bundle();
            bundle.putParcelable("idea", request);
            Navigation.findNavController(getView()).navigate(R.id.action_groupFragment_to_createIdeaFragment, bundle);
        }
    }

    @Override
    public void createViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(GroupViewModel.class);
    }

    @Override
    public void subscribe() {
        viewModel.getResult().observe(this, result -> {
            for (Map.Entry entry : result.entrySet()) {
                InputErrorLayout iel = (InputErrorLayout) llMembers.getFocusedChild();
                if (iel == null)
                    iel = (InputErrorLayout) llMembers.getChildAt(llMembers.getChildCount() - 2);
                if (entry.getValue() != null) {
                    if (iel.hasError())
                        iel.hideError();
                    Cocreator cocreator = (Cocreator) entry.getValue();
                    request.getCocreators().set(llMembers.indexOfChild(iel), cocreator);
                    iel.showClear(true);
                    iel.setEmployeeName(cocreator.getFirstName() + " " + cocreator.getLastName());
                } else {
                    iel.showClear(false);
                    iel.showError();
                }
            }
        });
    }

    private CreateIdeaRequest request;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_group;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        MainActivity activity = (MainActivity) getActivity();
        assert activity != null;
        activity.setupToolbar(getString(R.string.group_idea_title), false, false);
        activity.showToolbar(true);
        activity.hideReward();
        request = (CreateIdeaRequest) getArguments().get("idea");
        if (request.getCocreators().size() > 0)
            for (Cocreator cocreator : request.getCocreators()) {
                addMember(cocreator);
            }
        else {
            addEmptyMember();
            request.getCocreators().add(Cocreator.createEmpty());
        }
    }

    private void addEmptyMember() {
        InputErrorLayout inputErrorLayout = InputErrorLayout.createEmptyForGroup(getContext());
        addListeners(inputErrorLayout);
        llMembers.addView(inputErrorLayout, llMembers.getChildCount() - 1);
    }

    private void addMember(Cocreator cocreator) {
        InputErrorLayout inputErrorLayout = InputErrorLayout.createEmptyForGroup(getContext());
        inputErrorLayout.prefill(cocreator.getPersonalId());
        inputErrorLayout.showClear(true);
        inputErrorLayout.setEmployeeName(cocreator.getFirstName() + " " + cocreator.getLastName());
        addListeners(inputErrorLayout);
        llMembers.addView(inputErrorLayout, llMembers.getChildCount() - 1);
    }

    private void addListeners(InputErrorLayout inputErrorLayout) {
        inputErrorLayout.setTextWatcher(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!TextUtils.isEmpty(editable.toString())) {
                    viewModel.findUser(editable.toString(), inputErrorLayout.getId());
                } else {
                    inputErrorLayout.hideError();
                    request.getCocreators().set(llMembers.indexOfChild(inputErrorLayout), Cocreator.createEmpty());
                }
            }
        });
        inputErrorLayout.setListener(view -> {
            request.getCocreators().remove(llMembers.indexOfChild(view));
            llMembers.removeView(view);
            if (llMembers.getChildCount() == 1) {
                addEmptyMember();
                request.getCocreators().add(Cocreator.createEmpty());

            }
        });
    }

    private void filterSame() {
        Set<Cocreator> set = new LinkedHashSet<>(request.getCocreators());
        request.setCocreators(new ArrayList<>(set));
    }

    private boolean checkIfAllEmpty() {
        boolean allEmpty = true;
        for (Cocreator cocreator : request.getCocreators()) {
            if (!TextUtils.isEmpty(cocreator.getPersonalId())) {
                allEmpty = false;
            }
        }
        return allEmpty;
    }
}
