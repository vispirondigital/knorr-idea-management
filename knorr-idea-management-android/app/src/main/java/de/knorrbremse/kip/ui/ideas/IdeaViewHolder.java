package de.knorrbremse.kip.ui.ideas;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.knorrbremse.kip.R;
import de.knorrbremse.kip.custom.DateTimeUtils;
import de.knorrbremse.kip.data.Constants;
import de.knorrbremse.kip.data.Idea;
import de.knorrbremse.kip.data.IdeaResponse;
import de.knorrbremse.kip.data.enums.IdeaType;
import de.knorrbremse.kip.data.enums.Status;

public class IdeaViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tv_idea_name)
    TextView tvIdeaName;

    @BindView(R.id.tv_date)
    TextView tvDate;

    @BindView(R.id.tv_status)
    TextView tvStatus;

    @BindView(R.id.iv_status)
    ImageView ivStatus;

    @BindView(R.id.iv_person)
    ImageView ivPerson;

    public IdeaViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bindData(IdeaResponse idea) {
        tvIdeaName.setText(idea.getName());
        tvDate.setText(DateTimeUtils.getIdeadateFormat(idea.getCreatedAt()));
        if (idea.getIdeaType() == IdeaType.GROUP)
            ivPerson.setImageResource(R.drawable.ic_group_dark_grey_24dp);
        else ivPerson.setImageResource(R.drawable.ic_person_dark_grey_24dp);
        if (idea.getBonus() >= Constants.BONUS_MID) {
            ivStatus.setImageResource(R.drawable.ic_accepted_20dp);
        } else if (idea.getBonus() == Constants.BONUS_LOW) {
            ivStatus.setImageResource(R.drawable.ic_accepted_gold);
        } else {
            ivStatus.setImageResource(R.drawable.ic_declined_20dp);
        }
        tvStatus.setText(idea.getStatus().getString());
    }
}

