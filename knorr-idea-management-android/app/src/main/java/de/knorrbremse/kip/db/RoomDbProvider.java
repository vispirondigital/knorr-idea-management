package de.knorrbremse.kip.db;

import java.util.List;

import de.knorrbremse.kip.data.Cocreator;
import de.knorrbremse.kip.db.dao.EmployeesDao;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class RoomDbProvider {

    private final KipDatabase db;
    private final EmployeesDao employeesDao;

    public RoomDbProvider(KipDatabase db) {
        this.db = db;
        this.employeesDao = db.employeesDao();
    }

    /**
     * Employees
     */

    public Completable saveEmployee(Cocreator cocreator) {
        return Completable.create(emitter -> {
            try {
                employeesDao.insertEmployee(cocreator);
                emitter.onComplete();
            } catch (Exception e) {
                emitter.onError(e);
            }
        });
    }

    public Completable saveMultiple(List<Cocreator> cocreators) {
        return Completable.create(emitter -> {
            try {
                employeesDao.insertMultiple(cocreators);
                emitter.onComplete();
            } catch (Exception e) {
                emitter.onError(e);
            }
        });
    }

    public Completable updateEmployee(Cocreator cocreator) {
        return Completable.create(emitter -> {
            try {
                employeesDao.updateEmployee(cocreator);
                emitter.onComplete();
            } catch (Exception e) {
                emitter.onError(e);
            }
        });
    }

    public Single<Cocreator> getCocreator(String id) {
        return employeesDao.getEmployeeById(id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}
