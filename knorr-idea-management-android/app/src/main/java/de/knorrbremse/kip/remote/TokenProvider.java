package de.knorrbremse.kip.remote;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoDevice;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ChallengeContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import de.knorrbremse.kip.BuildConfig;
import de.knorrbremse.kip.custom.KeyHelper;
import de.knorrbremse.kip.custom.SingleLiveEvent;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class TokenProvider {

    private CognitoUserPool userPool;
    private KeyHelper keyHelper;
    private Context context;

    private final String ACCESS_TOKEN_KEY = "ktn_asc";

    public TokenProvider(Context context, KeyHelper keyHelper, CognitoUserPool userPool) {
        this.context = context;
        this.keyHelper = keyHelper;
        this.userPool = userPool;
    }

    public void storeToken(String token) {
        SharedPreferences prefs = context.getSharedPreferences(BuildConfig.PREFS, Context.MODE_PRIVATE);
        try {
            prefs.edit().putString(ACCESS_TOKEN_KEY, keyHelper.encrypt(context, token)).apply();
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | NoSuchProviderException | BadPaddingException | UnsupportedEncodingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }

    public String getToken() {
        SharedPreferences prefs = context.getSharedPreferences(BuildConfig.PREFS, Context.MODE_PRIVATE);
        try {
            return keyHelper.decrypt(context, prefs.getString(ACCESS_TOKEN_KEY, null));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | NoSuchProviderException | BadPaddingException | UnsupportedEncodingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void refreshTokenInBackground(@NonNull TokenRefreshListener listener) {
        CognitoUser user = userPool.getCurrentUser();
        user.getSessionInBackground(new AuthenticationHandler() {
            @Override
            public void onSuccess(CognitoUserSession userSession, CognitoDevice newDevice) {
                storeToken(userSession.getIdToken().getJWTToken());
                listener.onSuccess();
            }

            @Override
            public void getAuthenticationDetails(AuthenticationContinuation authenticationContinuation, String userId) {
                AuthenticationDetails authenticationDetails = null;
                try {
                    authenticationDetails = new AuthenticationDetails(userId, keyHelper.getPasswordFromPrefs(context), null);

                    // Pass the user sign-in credentials to the continuation
                    authenticationContinuation.setAuthenticationDetails(authenticationDetails);

                    // Allow the sign-in to continue
                    authenticationContinuation.continueTask();
                } catch (NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchProviderException | UnsupportedEncodingException e) {
                    e.printStackTrace();
                    listener.onFailure(e);
                }
            }

            @Override
            public void getMFACode(MultiFactorAuthenticationContinuation continuation) {

            }

            @Override
            public void authenticationChallenge(ChallengeContinuation continuation) {

            }

            @Override
            public void onFailure(Exception exception) {
                listener.onFailure(exception);
                Timber.e(exception);
            }
        });
    }

    public Flowable<Object> refreshTokenWithRx() {
        return Flowable.create(emitter -> {
            CognitoUser user = userPool.getCurrentUser();
            user.getSession(new AuthenticationHandler() {
                @Override
                public void onSuccess(CognitoUserSession userSession, CognitoDevice newDevice) {
                    storeToken(userSession.getIdToken().getJWTToken());
                    emitter.onNext("");
                }

                @Override
                public void getAuthenticationDetails(AuthenticationContinuation authenticationContinuation, String userId) {
                    AuthenticationDetails authenticationDetails = null;
                    try {
                        authenticationDetails = new AuthenticationDetails(userId, keyHelper.getPasswordFromPrefs(context), null);

                        // Pass the user sign-in credentials to the continuation
                        authenticationContinuation.setAuthenticationDetails(authenticationDetails);

                        // Allow the sign-in to continue
                        authenticationContinuation.continueTask();
                    } catch (NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchProviderException | UnsupportedEncodingException e) {
                        e.printStackTrace();
                        emitter.onError(e);

                    }
                }

                @Override
                public void getMFACode(MultiFactorAuthenticationContinuation continuation) {

                }

                @Override
                public void authenticationChallenge(ChallengeContinuation continuation) {

                }

                @Override
                public void onFailure(Exception exception) {
                    emitter.onError(exception);
                }
            });
        }, BackpressureStrategy.BUFFER).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public interface TokenRefreshListener {
        void onSuccess();

        void onFailure(Exception exception);
    }
}
