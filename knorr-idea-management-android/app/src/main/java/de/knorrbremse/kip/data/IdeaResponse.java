package de.knorrbremse.kip.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import de.knorrbremse.kip.data.enums.ActualPhase;
import de.knorrbremse.kip.data.enums.Category;
import de.knorrbremse.kip.data.enums.IdeaType;
import de.knorrbremse.kip.data.enums.Status;
import de.knorrbremse.kip.data.enums.WorkArea;

public class IdeaResponse implements Parcelable {

    @SerializedName("id")
    private
    String id;

    @SerializedName("createdBy")
    private
    String createdBy;

    @SerializedName("createdAt")
    private
    long createdAt;

    @SerializedName("description")
    private
    String description;

    @SerializedName("name")
    private
    String name;

    @SerializedName("status")
    private
    Status status;

    @SerializedName("implementationStatus")
    private
    ActualPhase actualPhase;

    @SerializedName("ideaType")
    private
    IdeaType ideaType;

    @SerializedName("category")
    private
    List<Category> category;

    @SerializedName("cocreators")
    private
    List<Cocreator> cocreators;

    @SerializedName("workingArea")
    private
    List<WorkArea> workingArea;

    @SerializedName("approverComment")
    private
    String approverComment;

    @SerializedName("approvedBy")
    private
    Cocreator approvedBy;

    @SerializedName("bonus")
    private
    int bonus;

    @SerializedName("logs")
    private
    List<Log> logs;


    public IdeaResponse(String id, String createdBy, long createdAt, String description, String name,
                        Status status, ActualPhase actualPhase, IdeaType ideaType, List<Category> category,
                        List<Cocreator> cocreators, List<WorkArea> workingArea, String approverComment,
                        Cocreator approvedBy, int bonus, List<Log> logs) {
        this.id = id;
        this.createdBy = createdBy;
        this.createdAt = createdAt;
        this.description = description;
        this.name = name;
        this.status = status;
        this.actualPhase = actualPhase;
        this.ideaType = ideaType;
        this.category = category;
        this.cocreators = cocreators;
        this.workingArea = workingArea;
        this.approverComment = approverComment;
        this.approvedBy = approvedBy;
        this.bonus = bonus;
        this.logs = logs;
    }

    protected IdeaResponse(Parcel in) {
        id = in.readString();
        createdBy = in.readString();
        createdAt = in.readLong();
        description = in.readString();
        name = in.readString();
        status = in.readParcelable(Status.class.getClassLoader());
        actualPhase = in.readParcelable(ActualPhase.class.getClassLoader());
        ideaType = in.readParcelable(IdeaType.class.getClassLoader());
        category = in.createTypedArrayList(Category.CREATOR);
        cocreators = in.createTypedArrayList(Cocreator.CREATOR);
        workingArea = in.createTypedArrayList(WorkArea.CREATOR);
        approverComment = in.readString();
        approvedBy = in.readParcelable(Cocreator.class.getClassLoader());
        bonus = in.readInt();
        logs = in.createTypedArrayList(Log.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(createdBy);
        dest.writeLong(createdAt);
        dest.writeString(description);
        dest.writeString(name);
        dest.writeParcelable(status, flags);
        dest.writeParcelable(actualPhase, flags);
        dest.writeParcelable(ideaType, flags);
        dest.writeTypedList(category);
        dest.writeTypedList(cocreators);
        dest.writeTypedList(workingArea);
        dest.writeString(approverComment);
        dest.writeParcelable(approvedBy, flags);
        dest.writeInt(bonus);
        dest.writeTypedList(logs);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<IdeaResponse> CREATOR = new Creator<IdeaResponse>() {
        @Override
        public IdeaResponse createFromParcel(Parcel in) {
            return new IdeaResponse(in);
        }

        @Override
        public IdeaResponse[] newArray(int size) {
            return new IdeaResponse[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ActualPhase getActualPhase() {
        return actualPhase;
    }

    public void setActualPhase(ActualPhase actualPhase) {
        this.actualPhase = actualPhase;
    }

    public IdeaType getIdeaType() {
        return ideaType;
    }

    public void setIdeaType(IdeaType ideaType) {
        this.ideaType = ideaType;
    }

    public List<Category> getCategory() {
        return category;
    }

    public void setCategory(List<Category> category) {
        this.category = category;
    }

    public List<Cocreator> getCocreators() {
        return cocreators;
    }

    public void setCocreators(List<Cocreator> cocreators) {
        this.cocreators = cocreators;
    }

    public List<WorkArea> getWorkingArea() {
        return workingArea;
    }

    public void setWorkingArea(List<WorkArea> workingArea) {
        this.workingArea = workingArea;
    }

    public String getApproverComment() {
        return approverComment;
    }

    public void setApproverComment(String approverComment) {
        this.approverComment = approverComment;
    }

    public Cocreator getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(Cocreator approvedBy) {
        this.approvedBy = approvedBy;
    }

    public int getBonus() {
        return bonus;
    }

    public void setBonus(int bonus) {
        this.bonus = bonus;
    }

    public List<Log> getLogs() {
        return logs;
    }

    public void setLogs(List<Log> logs) {
        this.logs = logs;
    }
}
