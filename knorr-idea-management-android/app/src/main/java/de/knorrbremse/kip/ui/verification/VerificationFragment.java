package de.knorrbremse.kip.ui.verification;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.cognitoidentityprovider.model.UserNotConfirmedException;

import androidx.navigation.Navigation;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.knorrbremse.kip.R;
import de.knorrbremse.kip.data.Constants;
import de.knorrbremse.kip.ui.BaseViewModelFragment;
import de.knorrbremse.kip.ui.custom.SnackbarHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class VerificationFragment extends BaseViewModelFragment<VerificationViewModel> {

    @OnClick(R.id.btn_next)
    public void next() {
        viewModel.checkConfirmation(id, password);
        activity.showLoading(true);
    }

    @BindView(R.id.cl_snackbar)
    CoordinatorLayout clSnackbar;

    private String id;
    private String password;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_verification, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        assert getArguments() != null;
        id = getArguments().getString("id");
        password = getArguments().getString("password");

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        subscribe();
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_verification;
    }

    @Override
    public void createViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(VerificationViewModel.class);
    }

    @Override
    public void subscribe() {
        viewModel.getConfirmed().observe(this, confirmed -> {
            activity.showLoading(false);
            Navigation.findNavController(getView()).navigate(VerificationFragmentDirections.verificationToIdeas());
        });
        viewModel.getException().observe(this, exception -> {
            activity.showLoading(false);
            if (exception instanceof UserNotConfirmedException)
                Toast.makeText(getContext(), R.string.unconfirmed_email, Toast.LENGTH_LONG).show();
            if (exception instanceof AmazonClientException) {
                if (exception.getMessage().contains(Constants.AMAZON_NO_INTERNET_EXCEPTION)) {
                    SnackbarHelper.createFloatingSnackbar(getContext(),clSnackbar);
                }
            }
        });
    }
}
