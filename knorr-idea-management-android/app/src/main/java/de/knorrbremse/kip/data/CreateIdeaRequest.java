package de.knorrbremse.kip.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import de.knorrbremse.kip.data.enums.Category;
import de.knorrbremse.kip.data.enums.IdeaType;
import de.knorrbremse.kip.data.enums.WorkArea;

public class CreateIdeaRequest implements Parcelable {

    @SerializedName("category")
    private
    List<Category> categories;

    @SerializedName("cocreators")
    private
    List<Cocreator> cocreators;

    @SerializedName("description")
    private
    String description;

    @SerializedName("name")
    private
    String name;

    @SerializedName("ideaType")
    private
    IdeaType ideaType;

    @SerializedName("workingArea")
    private
    List<WorkArea> workingAreas;

    public CreateIdeaRequest(List<Category> categories, List<Cocreator> cocreators, String description, String name, IdeaType ideaType, List<WorkArea> workingAreas) {
        this.categories = categories;
        this.cocreators = cocreators;
        this.description = description;
        this.name = name;
        this.ideaType = ideaType;
        this.workingAreas = workingAreas;
    }

    protected CreateIdeaRequest(Parcel in) {
        categories = in.createTypedArrayList(Category.CREATOR);
        cocreators = in.createTypedArrayList(Cocreator.CREATOR);
        description = in.readString();
        name = in.readString();
        ideaType = in.readParcelable(IdeaType.class.getClassLoader());
        workingAreas = in.createTypedArrayList(WorkArea.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(categories);
        dest.writeTypedList(cocreators);
        dest.writeString(description);
        dest.writeString(name);
        dest.writeParcelable(ideaType, flags);
        dest.writeTypedList(workingAreas);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CreateIdeaRequest> CREATOR = new Creator<CreateIdeaRequest>() {
        @Override
        public CreateIdeaRequest createFromParcel(Parcel in) {
            return new CreateIdeaRequest(in);
        }

        @Override
        public CreateIdeaRequest[] newArray(int size) {
            return new CreateIdeaRequest[size];
        }
    };

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<Cocreator> getCocreators() {
        return cocreators;
    }

    public void setCocreators(List<Cocreator> cocreators) {
        this.cocreators = cocreators;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public IdeaType getIdeaType() {
        return ideaType;
    }

    public void setIdeaType(IdeaType ideaType) {
        this.ideaType = ideaType;
    }

    public List<WorkArea> getWorkingAreas() {
        return workingAreas;
    }

    public void setWorkingAreas(List<WorkArea> workingAreas) {
        this.workingAreas = workingAreas;
    }
}
