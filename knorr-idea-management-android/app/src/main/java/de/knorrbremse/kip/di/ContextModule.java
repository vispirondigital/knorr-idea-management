package de.knorrbremse.kip.di;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import de.knorrbremse.kip.KipApp;

@Module
public class ContextModule {
    private final KipApp application;

    public ContextModule(KipApp application) {
        this.application = application;
    }

    @Provides
    @ApplicationScope
    public Context context(){
        return application.getApplicationContext();
    }

    @Provides
    @ApplicationScope
    public KipApp app(){
        return application;
    }


}
