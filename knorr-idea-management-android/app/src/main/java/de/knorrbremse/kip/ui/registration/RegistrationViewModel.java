package de.knorrbremse.kip.ui.registration;


import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserAttributes;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserCodeDeliveryDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.SignUpHandler;

import javax.inject.Inject;

import de.knorrbremse.kip.custom.SingleLiveEvent;
import de.knorrbremse.kip.viewmodel.BaseViewModel;
import timber.log.Timber;

public class RegistrationViewModel extends BaseViewModel implements SignUpHandler {

    @Inject
    CognitoUserPool userPool;

    @Inject
    public RegistrationViewModel() {
    }

    private SingleLiveEvent<CognitoUser> userSignUp = new SingleLiveEvent<>();

    public SingleLiveEvent<Exception> getException() {
        return exception;
    }

    private SingleLiveEvent<Exception> exception = new SingleLiveEvent<>();

    SingleLiveEvent<CognitoUser> getUserSignUp() {
        return userSignUp;
    }

    public void register(String email, String personalId, String password) {
        CognitoUserAttributes attrs = new CognitoUserAttributes();
        attrs.addAttribute("email", email);
        attrs.addAttribute("custom:personalId", personalId);
        userPool.signUpInBackground(email, password, attrs, null, this);
    }

    @Override
    public void onSuccess(CognitoUser user, boolean signUpConfirmationState, CognitoUserCodeDeliveryDetails cognitoUserCodeDeliveryDetails) {
        Timber.e(user.getUserId());
        userSignUp.postValue(user);
    }

    @Override
    public void onFailure(Exception e) {
        Timber.e(e);
        exception.postValue(e);
    }
}
