package de.knorrbremse.kip.remote.service;

import de.knorrbremse.kip.data.CreateIdeaRequest;
import de.knorrbremse.kip.data.CreateIdeaResponse;
import de.knorrbremse.kip.data.EmployeesLambdaResponse;
import de.knorrbremse.kip.data.IdeasLambdaResponse;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface IdeasService {

    @Headers("Content-Type: application/json")
    @GET("idea")
    Single<IdeasLambdaResponse> getIdeas();

    @Headers("Content-Type: application/create-idea")
    @POST("idea")
    Single<CreateIdeaResponse> createIdea(@Body CreateIdeaRequest idea);

    @Headers("Content-Type: application/json")
    @GET("employees")
    Single<EmployeesLambdaResponse> getEmployees();
}
