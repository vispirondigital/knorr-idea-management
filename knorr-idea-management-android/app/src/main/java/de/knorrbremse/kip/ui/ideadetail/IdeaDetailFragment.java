package de.knorrbremse.kip.ui.ideadetail;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.sql.Date;
import java.util.List;

import butterknife.BindView;
import de.knorrbremse.kip.MainActivity;
import de.knorrbremse.kip.R;
import de.knorrbremse.kip.custom.DateTimeUtils;
import de.knorrbremse.kip.data.ChangeLog;
import de.knorrbremse.kip.data.Cocreator;
import de.knorrbremse.kip.data.Constants;
import de.knorrbremse.kip.data.IdeaResponse;
import de.knorrbremse.kip.data.Log;
import de.knorrbremse.kip.data.enums.ActualPhase;
import de.knorrbremse.kip.data.enums.IdeaType;
import de.knorrbremse.kip.ui.BaseViewModelFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class IdeaDetailFragment extends BaseViewModelFragment<IdeaDetailViewModel> {

    @BindView(R.id.tv_actual_status)
    TextView tvStatus;

    @BindView(R.id.tv_actual_status_detail)
    TextView tvStatusDetail;

    @BindView(R.id.tv_reason)
    TextView tvReason;

    @BindView(R.id.tv_history)
    TextView tvHistory;

    @BindView(R.id.tv_description)
    TextView tvDescription;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        assert getArguments() != null;
        IdeaResponse idea = (IdeaResponse) getArguments().get("idea");
        MainActivity activity = (MainActivity) getActivity();
        assert activity != null;
        activity.setupToolbar(idea.getName(), true, false);
        if (idea.getBonus() >= Constants.BONUS_LOW) {
            activity.showReward(idea.getBonus());
        }

        List<Log> logs = idea.getLogs();
        String approver = "";
        if (idea.getApprovedBy() != null) {
            approver = !TextUtils.isEmpty(idea.getApprovedBy().getFirstName()) ?
                    idea.getApprovedBy().getFirstName() + " " + idea.getApprovedBy().getLastName() : "";
        }
        tvStatusDetail.setText(getString(R.string.status, getString(idea.getStatus().getString())) + "\n"
                + getString(R.string.processed_by, approver));
        tvReason.setText(TextUtils.isEmpty(idea.getApproverComment()) ? "" : idea.getApproverComment());
        StringBuilder builder = new StringBuilder();

        for (int i = logs.size() - 1; i >= 0; i--) {
            Log log = logs.get(i);
            ChangeLog changeLog = log.getChangeLog();
            if (changeLog.getImplementationStatus() != null) {
                ActualPhase phase = ActualPhase.fromString(changeLog.getImplementationStatus().getNewValue());
                String date = DateTimeUtils.getIdeadateFormat(log.getCreatedAt());
                builder.append(getString(phase.getString()));
                builder.append(": ");
                builder.append(date);
                builder.append("\n");
            }
        }
        if (idea.getIdeaType() == IdeaType.GROUP) {
            builder.append(getString(R.string.created_with));
            builder.append(" ");
            for (Cocreator cocreator : idea.getCocreators()) {
                builder.append(cocreator.getFirstName());
                builder.append(" ");
                builder.append(cocreator.getLastName());
                builder.append(", ");
            }
            builder.trimToSize();
            builder.delete(builder.length() - 2, builder.length() - 1);
        }
        tvHistory.setText(builder.toString());
        tvDescription.setText(idea.getDescription());

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_idea_detail;
    }

    @Override
    public void createViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(IdeaDetailViewModel.class);
    }

    @Override
    public void subscribe() {

    }

}
