package de.knorrbremse.kip.ui.registration;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.cognitoidentityprovider.model.UserLambdaValidationException;
import com.amazonaws.services.cognitoidentityprovider.model.UsernameExistsException;

import androidx.navigation.Navigation;
import butterknife.BindView;
import butterknife.OnClick;
import de.knorrbremse.kip.BuildConfig;
import de.knorrbremse.kip.R;
import de.knorrbremse.kip.data.Constants;
import de.knorrbremse.kip.ui.BaseViewModelFragment;
import de.knorrbremse.kip.ui.custom.InputErrorLayout;
import de.knorrbremse.kip.ui.custom.SnackbarHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationFragment extends BaseViewModelFragment<RegistrationViewModel> {

    @OnClick(R.id.btn_register)
    public void register() {
        if (checkFields()) {
            activity.showLoading(true);
            password = ielPassword.getText();
            viewModel.register(ielEmail.getText(), ielPersonalNumber.getText(), password);
        }
    }

    @OnClick(R.id.tv_agb)
    public void toAgb() {
        Navigation.findNavController(getView()).navigate(R.id.action_registrationFragment_to_AGBFragment);
    }

    @BindView(R.id.iel_email)
    InputErrorLayout ielEmail;

    @BindView(R.id.iel_personal_number)
    InputErrorLayout ielPersonalNumber;

    @BindView(R.id.iel_password)
    InputErrorLayout ielPassword;

    @BindView(R.id.iel_repeat_password)
    InputErrorLayout ielRepeatPassword;

    @BindView(R.id.cb_agb)
    CheckBox cbAgb;

    @BindView(R.id.tv_terms_conditions)
    TextView tvTermsConditions;

    @BindView(R.id.cl_snackbar)
    CoordinatorLayout clSnackbar;

    private String password;
    private String prefillEmail;
    private String email;
    private String personalNumber;
    private String repeatPassword;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefillEmail = BuildConfig.EMAIL;
        ielEmail.prefill(prefillEmail);
        setTextWatchers();
    }


    @Override
    public int getLayoutId() {
        return R.layout.fragment_registration;
    }

    @Override
    public void createViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(RegistrationViewModel.class);
    }

    @Override
    public void subscribe() {
        viewModel.getUserSignUp().observe(this, cognitoUser -> {
            if (cognitoUser != null) {
                Bundle bundle = new Bundle();
                bundle.putString("id", cognitoUser.getUserId());
                bundle.putString("password", password);
                activity.showLoading(false);
                Navigation.findNavController(getView()).navigate(R.id.register, bundle);
            }
        });
        viewModel.getException().observe(this, exception -> {
            activity.showLoading(false);
            if (exception instanceof UserLambdaValidationException) {
                ielEmail.setError(R.string.unauthorized);
                ielEmail.showError();
            } else if (exception instanceof UsernameExistsException) {
                ielEmail.setError(R.string.user_already_exists);
                ielEmail.showError();
            } else if (exception instanceof AmazonClientException) {
                if (exception.getMessage().contains(Constants.AMAZON_NO_INTERNET_EXCEPTION)) {
                    SnackbarHelper.createFloatingSnackbar(getContext(),clSnackbar);
                } else {
                    SnackbarHelper.createFloatingSnackbar(getContext(),clSnackbar,getString(R.string.register_unexpected_error));
                }
            } else {
                SnackbarHelper.createFloatingSnackbar(getContext(),clSnackbar,getString(R.string.register_unexpected_error));
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        setTextWatchers();
        if (!TextUtils.isEmpty(email))
            ielEmail.prefill(email);
        if (!TextUtils.isEmpty(personalNumber))
            ielPersonalNumber.prefill(personalNumber);
        if (!TextUtils.isEmpty(password))
            ielPassword.prefill(password);
        if (!TextUtils.isEmpty(repeatPassword))
            ielRepeatPassword.prefill(repeatPassword);
    }

    @Override
    public void onPause() {
        super.onPause();
        ielEmail.removeTextWatcher();
        ielPersonalNumber.removeTextWatcher();
        ielPassword.removeTextWatcher();
        ielRepeatPassword.removeTextWatcher();
        if (!ielEmail.getText().equals(prefillEmail)) {
            email = ielEmail.getText();
        }
        personalNumber = ielPersonalNumber.getText();
        password = ielPassword.getText();
        repeatPassword = ielRepeatPassword.getText();
    }

    /**
     * Setting the TextWatchers for input fields
     */
    private void setTextWatchers() {
        ielEmail.setTextWatcher(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().endsWith(prefillEmail)) {
                    ielEmail.prefill(prefillEmail);
                }
                if (!editable.toString().equals(prefillEmail)) {
                    ielEmail.hideError();
                }
            }
        });

        ielPersonalNumber.setTextWatcher(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (ielPersonalNumber.hasError() && !TextUtils.isEmpty(editable))
                    ielPersonalNumber.hideError();
            }
        });
        ielRepeatPassword.setTextWatcher(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if ((TextUtils.isEmpty(editable) && !TextUtils.isEmpty(ielPassword.getText())) || !editable.toString().equals(ielPassword.getText())) {
                    ielRepeatPassword.showError();
                } else {
                    ielRepeatPassword.hideError();
                }
            }
        });
        ielPassword.setTextWatcher(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (ielPassword.hasError() && !TextUtils.isEmpty(editable) && editable.length() >= 8)
                    ielPassword.hideError();
                if (!TextUtils.isEmpty(ielRepeatPassword.getText()) && !editable.toString().equals(ielRepeatPassword.getText()))
                    ielRepeatPassword.showError();
            }
        });
    }

    /**
     * Method used to check if all fields are filled in properly
     */
    private boolean checkFields() {
        boolean register = true;
        if (ielEmail.getText().equals(prefillEmail)) {
            register = false;
            ielEmail.setError(R.string.email_not_empty);
            ielEmail.showError();
        }
        if (TextUtils.isEmpty(ielPersonalNumber.getText())) {
            register = false;
            ielPersonalNumber.showError();
        }
        if (TextUtils.isEmpty(ielPassword.getText()) || ielPassword.getText().length() < 8) {
            register = false;
            ielPassword.showError();
        }
        if (TextUtils.isEmpty(ielRepeatPassword.getText()) ||
                (!TextUtils.isEmpty(ielPassword.getText()) &&
                        !ielRepeatPassword.getText().equals(ielPassword.getText()))) {
            register = false;
            ielRepeatPassword.showError();
        }
        if (!cbAgb.isChecked()) {
            register = false;
            tvTermsConditions.setTextColor(getResources().getColor(R.color.red));
        }
        return register;
    }
}
