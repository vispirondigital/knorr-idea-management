package de.knorrbremse.kip.ui.createidea;

import javax.inject.Inject;

import de.knorrbremse.kip.custom.SingleLiveEvent;
import de.knorrbremse.kip.data.CreateIdeaRequest;
import de.knorrbremse.kip.data.CreateIdeaResponse;
import de.knorrbremse.kip.remote.TokenProvider;
import de.knorrbremse.kip.remote.api.IdeaApi;
import de.knorrbremse.kip.viewmodel.BaseViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class CreateIdeaViewModel extends BaseViewModel {

    @Inject
    CreateIdeaViewModel() {
    }

    @Inject
    IdeaApi ideasApi;

    @Inject
    TokenProvider tokenProvider;

    private SingleLiveEvent<CreateIdeaResponse> ideaCreated = new SingleLiveEvent<>();

    private SingleLiveEvent<Throwable> onError = new SingleLiveEvent<>();

    public SingleLiveEvent<CreateIdeaResponse> getIdeaCreated() {
        return ideaCreated;
    }

    public SingleLiveEvent<Throwable> getOnError() {
        return onError;
    }

    public void createIdea(CreateIdeaRequest idea) {
        addDisposable(ideasApi.createIdea(idea).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe((response, throwable) -> {
            if (response != null) {
                if (response.statusCode == 200) {
                    ideaCreated.postValue(response);
                } else if (response.statusCode == 401) {
                    tokenProvider.refreshTokenInBackground(new TokenProvider.TokenRefreshListener() {
                        @Override
                        public void onSuccess() {
                            createIdea(idea);
                        }

                        @Override
                        public void onFailure(Exception exception) {
                            onError.postValue(exception);
                        }
                    });
                }
            } else {
                Timber.e(throwable);
                onError.postValue(throwable);
            }
        }));
    }
}
