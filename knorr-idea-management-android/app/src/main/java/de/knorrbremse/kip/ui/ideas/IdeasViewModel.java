package de.knorrbremse.kip.ui.ideas;

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GenericHandler;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GetDetailsHandler;

import java.util.List;

import javax.inject.Inject;

import de.knorrbremse.kip.custom.SingleLiveEvent;
import de.knorrbremse.kip.data.IdeaResponse;
import de.knorrbremse.kip.remote.TokenProvider;
import de.knorrbremse.kip.data.IdeasLambdaResponse;
import de.knorrbremse.kip.db.RoomDbProvider;
import de.knorrbremse.kip.remote.api.IdeaApi;
import de.knorrbremse.kip.viewmodel.BaseViewModel;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class IdeasViewModel extends BaseViewModel implements GenericHandler {

    @Inject
    public IdeasViewModel() {
    }

    @Inject
    CognitoUserPool userPool;

    @Inject
    TokenProvider tokenProvider;

    @Inject
    IdeaApi ideasApi;

    @Inject
    RoomDbProvider provider;

    private SingleLiveEvent<Boolean> loggedOut = new SingleLiveEvent<>();

    private SingleLiveEvent<List<IdeaResponse>> ideas = new SingleLiveEvent<>();

    private SingleLiveEvent<Throwable> onError = new SingleLiveEvent<>();

    private SingleLiveEvent<String> email = new SingleLiveEvent<>();

    public SingleLiveEvent<Boolean> getLoggedOut() {
        return loggedOut;
    }

    public SingleLiveEvent<List<IdeaResponse>> getIdeas() {
        return ideas;
    }

    public SingleLiveEvent<Throwable> getOnError() {
        return onError;
    }

    public SingleLiveEvent<String> getEmail() {
        return email;
    }

    public void logOut() {
        CognitoUser user = userPool.getCurrentUser();
        user.globalSignOutInBackground(this);

    }

    private Single<IdeasLambdaResponse> loadIdeas() {
        return ideasApi.getIdeas().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    private Completable getEmployees() {
        return ideasApi.getEmployees().map(response -> {
            if (response.statusCode == 200)
                return response.body;
            else return null;
        }).flatMapCompletable(result ->
                provider.saveMultiple(result)).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public void refresh() {
        addDisposable(Single.zip(loadIdeas(), getEmployees().toSingleDefault(""),
                (response, ignore) -> response).subscribe((result, throwable) -> {
            if (throwable == null) {
                ideas.postValue(result.body);
            } else {
                onError.postValue(throwable);
                Timber.e(throwable);
            }

        }));
    }

    public void getUserName() {
        CognitoUser user = userPool.getCurrentUser();
        user.getDetailsInBackground(new GetDetailsHandler() {
            @Override
            public void onSuccess(CognitoUserDetails cognitoUserDetails) {
                email.postValue(cognitoUserDetails.getAttributes().getAttributes().get("email"));
            }

            @Override
            public void onFailure(Exception exception) {

            }
        });
    }

    @Override
    public void onSuccess() {
        loggedOut.postValue(true);
    }

    @Override
    public void onFailure(Exception exception) {
        Timber.e(exception);
        onError.postValue(exception);
    }
}
