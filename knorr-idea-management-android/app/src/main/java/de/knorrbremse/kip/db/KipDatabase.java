package de.knorrbremse.kip.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import de.knorrbremse.kip.data.Cocreator;
import de.knorrbremse.kip.db.dao.EmployeesDao;

@Database(entities = {Cocreator.class}, version = 1, exportSchema = false)
public abstract class KipDatabase extends RoomDatabase {

    private static KipDatabase INSTANCE = null;

    public static KipDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (KipDatabase.class) {
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(), KipDatabase.class,
                        "kipData.db")
                        .fallbackToDestructiveMigration()
                        .build();
            }
        }
        return INSTANCE;
    }

    public void destroyInstance() {
        INSTANCE = null;
    }

    abstract EmployeesDao employeesDao();
}
