package de.knorrbremse.kip.data.enums;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.StringRes;

import java.util.HashMap;
import java.util.Map;

import de.knorrbremse.kip.R;

public enum Status implements Parcelable {


    OPEN(1, R.string.status_open),
    ACCEPTED(2, R.string.accepted),
    REJECTED(3, R.string.declined);

    private static Map<Integer, Status> statuses;

    int status;
    @StringRes
    int string;

    static {
        statuses = new HashMap<>();
        for (Status status : values()) {
            statuses.put(status.status, status);
        }
    }

    Status(int status, @StringRes int string) {
        this.status = status;
        this.string = string;
    }

    Status(Parcel in) {
        status = in.readInt();
    }

    public static final Creator<Status> CREATOR = new Creator<Status>() {
        @Override
        public Status createFromParcel(Parcel in) {
            return Status.fromInt(in.readInt());
        }

        @Override
        public Status[] newArray(int size) {
            return new Status[size];
        }
    };

    public static Status fromInt(int status) {
        return statuses.get(status);
    }

    public @StringRes
    int getString() {
        return string;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(status);
    }
}
