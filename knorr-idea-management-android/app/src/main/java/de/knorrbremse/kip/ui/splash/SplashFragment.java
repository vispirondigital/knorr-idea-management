package de.knorrbremse.kip.ui.splash;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import java.util.concurrent.TimeUnit;

import androidx.navigation.Navigation;
import butterknife.BindView;
import de.knorrbremse.kip.R;
import de.knorrbremse.kip.ui.BaseViewModelFragment;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;

/**
 * A simple {@link Fragment} subclass.
 */
public class SplashFragment extends BaseViewModelFragment<SplashViewModel> {

    @BindView(R.id.iv_logo)
    ImageView ivLogo;

    private Disposable disposable;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_splash;
    }

    @Override
    public void createViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SplashViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Animation rotation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate);
        rotation.setFillAfter(true);
        ivLogo.startAnimation(rotation);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        disposable = Single.timer(2000, TimeUnit.MILLISECONDS).subscribe(o -> {
            viewModel.checkLoggedIn();
        });

    }

    @Override
    public void subscribe() {
        viewModel.getLoggedIn().observe(this, loggedIn -> {
            if (loggedIn) {
                Navigation.findNavController(getView()).navigate(SplashFragmentDirections.actionSplashFragmentToIdeasFragment());
            } else
                Navigation.findNavController(getView()).navigate(SplashFragmentDirections.actionSplashFragmentToLoginFragment());
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        disposable.dispose();
    }

}
