package de.knorrbremse.kip.data;

public class Constants {

    /* Bonus values */
    public static final int BONUS_MID = 50;
    public static final int BONUS_LOW = 10;
    public static final int NO_BONUS = 0;
    public static final int HIGHEST_BONUS = 51;

    public static final String AMAZON_NO_INTERNET_EXCEPTION = "Unable to execute HTTP request";

    /* okhttp3.OkHttpClient */
    public static final String OK_HTTP_CLIENT_BASE_URL = " https://q4wpaqiv1j.execute-api.eu-central-1.amazonaws.com/STAGE/";//"https://dc3goghpmd.execute-api.eu-central-1.amazonaws.com/STAGE/";

    /* amazonaws CognitoUserPool */
    public static final String COGNITO_USER_POOL_ID = "eu-central-1_3cheFVYGm";//"eu-central-1_Dai8L7p2R";
    public static final String COGNITO_CLIENT_ID = "20n986tk0c43l13uk2d5huc3oe";//"472e838l53pntpsqcf8cpsktnr";
    public static final String COGNITO_CLIENT_SECRET = "j49a6k13s7bpfva6ha7l7elc11c6sofg6bbup5surkgq7mdqpg2";//"ui9r0leic0aj47c2iqen4bit8iauqu0fig2eunbbflvjeuhfueo";

}
