package de.knorrbremse.kip.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import de.knorrbremse.kip.ui.createidea.CreateIdeaViewModel;
import de.knorrbremse.kip.ui.group.GroupViewModel;
import de.knorrbremse.kip.ui.ideadetail.IdeaDetailViewModel;
import de.knorrbremse.kip.ui.ideas.IdeasViewModel;
import de.knorrbremse.kip.ui.login.LoginViewModel;
import de.knorrbremse.kip.ui.registration.RegistrationViewModel;
import de.knorrbremse.kip.ui.splash.SplashViewModel;
import de.knorrbremse.kip.ui.verification.VerificationViewModel;
import de.knorrbremse.kip.viewmodel.KipViewModelFactory;

@SuppressWarnings("unused")
@Module
public abstract class ViewModelModule {

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(KipViewModelFactory factory);

    @Binds
    @IntoMap
    @ApplicationScope
    @ViewModelKey(RegistrationViewModel.class)
    abstract ViewModel bindRegistrationViewModel(RegistrationViewModel registrationViewModel);

    @Binds
    @IntoMap
    @ApplicationScope
    @ViewModelKey(VerificationViewModel.class)
    abstract ViewModel bindVerificationViewModel(VerificationViewModel verificationViewModel);

    @Binds
    @IntoMap
    @ApplicationScope
    @ViewModelKey(LoginViewModel.class)
    abstract ViewModel bindLoginViewModel(LoginViewModel loginViewModel);

    @Binds
    @IntoMap
    @ApplicationScope
    @ViewModelKey(IdeasViewModel.class)
    abstract ViewModel bindIdeasViewModel(IdeasViewModel ideasViewModel);

    @Binds
    @IntoMap
    @ApplicationScope
    @ViewModelKey(IdeaDetailViewModel.class)
    abstract ViewModel bindIdeaDetailViewModel(IdeaDetailViewModel ideaDetailViewModel);

    @Binds
    @IntoMap
    @ApplicationScope
    @ViewModelKey(SplashViewModel.class)
    abstract ViewModel bindSplashViewModel(SplashViewModel splashViewModel);

    @Binds
    @IntoMap
    @ApplicationScope
    @ViewModelKey(CreateIdeaViewModel.class)
    abstract ViewModel bindCreateIdeaViewModel(CreateIdeaViewModel createIdeaViewModel);

    @Binds
    @IntoMap
    @ApplicationScope
    @ViewModelKey(GroupViewModel.class)
    abstract ViewModel bindGroupViewModel(GroupViewModel groupViewModel);
}
