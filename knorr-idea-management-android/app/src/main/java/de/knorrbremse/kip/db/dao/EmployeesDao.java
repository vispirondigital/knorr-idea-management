package de.knorrbremse.kip.db.dao;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import de.knorrbremse.kip.data.Cocreator;
import io.reactivex.Single;

@Dao
public interface EmployeesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertEmployee(Cocreator cocreator);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertMultiple(List<Cocreator> cocreators);

    @Update
    public void updateEmployee(Cocreator cocreator);

    @Query("SELECT * FROM employees WHERE personalId = :id")
    public Single<Cocreator> getEmployeeById(String id);
}
