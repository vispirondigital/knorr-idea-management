package de.knorrbremse.kip.remote;

import org.reactivestreams.Publisher;

import io.reactivex.Flowable;
import io.reactivex.functions.Function;
import retrofit2.adapter.rxjava2.HttpException;
import timber.log.Timber;

public class RetryWithNewToken implements Function<Flowable<Throwable>, Publisher<?>> {

    private final TokenProvider provider;

    public RetryWithNewToken(TokenProvider provider) {
        this.provider = provider;
    }

    @Override
    public Publisher<?> apply(Flowable<Throwable> flowable) {
        return flowable.flatMap(throwable -> {
            if (throwable instanceof HttpException) {
                try {
                    HttpException httpException = (HttpException) throwable;
                    if (httpException.code() == 401) {
                        return provider.refreshTokenWithRx();
                    }
                    return Flowable.error(throwable);
                } catch (Exception e) {
                    Timber.e(e);
                }
            }
            return Flowable.error(throwable);
        });
    }
}
