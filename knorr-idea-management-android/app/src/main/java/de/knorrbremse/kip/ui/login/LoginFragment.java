package de.knorrbremse.kip.ui.login;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.cognitoidentityprovider.model.NotAuthorizedException;
import com.amazonaws.services.cognitoidentityprovider.model.UserNotConfirmedException;
import com.amazonaws.services.cognitoidentityprovider.model.UserNotFoundException;

import androidx.navigation.Navigation;
import butterknife.BindView;
import butterknife.OnClick;
import de.knorrbremse.kip.BuildConfig;
import de.knorrbremse.kip.MainActivity;
import de.knorrbremse.kip.R;
import de.knorrbremse.kip.data.Constants;
import de.knorrbremse.kip.ui.BaseViewModelFragment;
import de.knorrbremse.kip.ui.custom.InputErrorLayout;
import de.knorrbremse.kip.ui.custom.SnackbarHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends BaseViewModelFragment<LoginViewModel> {

    @OnClick(R.id.btn_register)
    public void toRegistration() {
        Navigation.findNavController(getView()).navigate(R.id.to_registration);
    }

    @OnClick(R.id.btn_login)
    public void login() {
        if (checkLogin()) {
            viewModel.login(ielEmail.getText(), ielPassword.getText());
            activity.showLoading(true);
        }
    }

    @BindView(R.id.iel_email)
    InputErrorLayout ielEmail;

    @BindView(R.id.iel_password)
    InputErrorLayout ielPassword;

    @BindView(R.id.cl_snackbar)
    CoordinatorLayout clSnackbar;

    private String email;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        email = BuildConfig.EMAIL;
        ielEmail.prefill(email);
        setTextWatchers();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        MainActivity activity = (MainActivity) getActivity();
        assert activity != null;
        activity.showToolbar(false);
    }

    /**
     * Setting the TextWatchers for input fields
     */
    private void setTextWatchers() {
        ielEmail.setTextWatcher(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().endsWith(email)) {
                    ielEmail.prefill(email);
                }
            }
        });
        ielPassword.setTextWatcher(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (ielPassword.hasError() && !TextUtils.isEmpty(editable))
                    ielPassword.hideError();
            }
        });
    }

    /**
     * Method used to check if all fields are filled in properly
     */
    private boolean checkLogin() {
        boolean login = true;
        if (ielEmail.getText().equals(email)) {
            ielEmail.setError(R.string.email_not_empty);
            ielEmail.showError();
            login = false;
        }
        if (TextUtils.isEmpty(ielPassword.getText())) {
            ielPassword.setError(R.string.password_not_empty);
            ielPassword.showError();
            login = false;
        }
        return login;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_login;
    }

    @Override
    public void createViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(LoginViewModel.class);
    }

    @Override
    public void subscribe() {
        viewModel.getLoggedIn().observe(this, name -> {
            activity.showLoading(false);
            Navigation.findNavController(getView()).navigate(LoginFragmentDirections.loginToIdeas());
        });
        viewModel.getException().observe(this, exception -> {
            activity.showLoading(false);
            if (exception instanceof UserNotFoundException) {
                ielEmail.setError(R.string.user_not_exist);
                ielEmail.showError();
            }
            if (exception instanceof UserNotConfirmedException) {
                ielEmail.setError(R.string.unconfirmed_email);
                ielEmail.showError();
            }
            if (exception instanceof NotAuthorizedException) {
                ielPassword.setError(R.string.wrong_name_password);
                ielPassword.showError();
            }
            if (exception instanceof AmazonClientException) {
                if (exception.getMessage().contains(Constants.AMAZON_NO_INTERNET_EXCEPTION)) {
                    SnackbarHelper.createFloatingSnackbar(getContext(),clSnackbar);
                }
            }
            if (exception.getMessage().contains("timeout") || exception.getMessage().contains("Timeout")) {
                Toast.makeText(getContext(), "Timeout exception", Toast.LENGTH_LONG).show();
            }
        });
    }
}
