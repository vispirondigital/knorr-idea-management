package de.knorrbremse.kip.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EmployeesLambdaResponse {

    @SerializedName("statusCode")
    public int statusCode;

    @SerializedName("body")
    public List<Cocreator> body;
}
