package de.knorrbremse.kip.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import de.knorrbremse.kip.MainActivity;

@Module
@SuppressWarnings("unused")
public abstract class MainActivityModule {

    @ContributesAndroidInjector(modules = {FragmentBuildersModule.class})
    abstract MainActivity contributeMainActivity();
}
