package de.knorrbremse.kip.remote.api;

import de.knorrbremse.kip.data.CreateIdeaRequest;
import de.knorrbremse.kip.data.CreateIdeaResponse;
import de.knorrbremse.kip.data.EmployeesLambdaResponse;
import de.knorrbremse.kip.data.IdeasLambdaResponse;
import de.knorrbremse.kip.remote.RetryWithNewToken;
import de.knorrbremse.kip.remote.TokenProvider;
import de.knorrbremse.kip.remote.service.IdeasService;
import io.reactivex.Single;

public class IdeaApiImpl implements IdeaApi {

    private TokenProvider provider;
    private IdeasService ideasService;

    public IdeaApiImpl(IdeasService ideasService, TokenProvider provider) {
        this.ideasService = ideasService;
        this.provider = provider;
    }

    @Override
    public Single<IdeasLambdaResponse> getIdeas() {
        return ideasService.getIdeas().retryWhen(new RetryWithNewToken(provider));
    }

    @Override
    public Single<CreateIdeaResponse> createIdea(CreateIdeaRequest idea) {
        return ideasService.createIdea(idea).retryWhen(new RetryWithNewToken(provider));
    }

    @Override
    public Single<EmployeesLambdaResponse> getEmployees() {
        return ideasService.getEmployees().retryWhen(new RetryWithNewToken(provider));
    }
}
