package de.knorrbremse.kip.ui.categories;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.ScrollView;

import java.util.List;

import androidx.navigation.Navigation;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;
import de.knorrbremse.kip.MainActivity;
import de.knorrbremse.kip.R;
import de.knorrbremse.kip.data.CreateIdeaRequest;
import de.knorrbremse.kip.data.enums.Category;
import de.knorrbremse.kip.data.enums.WorkArea;
import de.knorrbremse.kip.ui.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoriesAreasFragment extends BaseFragment {

    @OnClick(R.id.btn_save)
    public void saveCategories() {
        if (checkFields()) {
            request.getCategories().clear();
            request.getWorkingAreas().clear();
            for (int i = 0; i < categories.size(); i++) {
                if (categories.get(i).isChecked())
                    request.getCategories().add(Category.fromInt(i));
            }
            for (int i = 0; i < workAreas.size(); i++) {
                if (workAreas.get(i).isChecked())
                    request.getWorkingAreas().add(WorkArea.fromInt(i));
            }
            Bundle bundle = new Bundle();
            bundle.putParcelable("idea", request);
            Navigation.findNavController(getView()).navigate(R.id.action_categoriesAreasFragment_to_createIdeaFragment, bundle);
        } else
            Snackbar.make(getView(), R.string.one_category_area, Snackbar.LENGTH_LONG).show();
    }

    @OnClick(R.id.btn_clear)
    public void clearSelection() {
        for (CheckBox chb : categories) {
            chb.setChecked(false);
        }
        for (CheckBox chb : workAreas) {
            chb.setChecked(false);
        }
    }

    @BindViews({R.id.chb_security, R.id.chb_quality, R.id.chb_productivity})
    List<CheckBox> categories;

    @BindViews({R.id.chb_01, R.id.chb_02, R.id.chb_03, R.id.chb_04, R.id.chb_05, R.id.chb_06,
            R.id.chb_07, R.id.chb_08, R.id.chb_09, R.id.chb_010, R.id.chb_011, R.id.chb_012,
            R.id.chb_013, R.id.chb_014, R.id.chb_015})
    List<CheckBox> workAreas;

    private CreateIdeaRequest request;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_categories_areas;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        MainActivity activity = (MainActivity) getActivity();
        assert activity != null;
        activity.setupToolbar(getString(R.string.categories_working_areas_title), false, false);
        activity.showToolbar(true);
        activity.hideReward();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        request = (CreateIdeaRequest) getArguments().get("idea");
        for (Category category : request.getCategories()) {
            categories.get(category.getValue()).setChecked(true);
        }
        for (WorkArea area : request.getWorkingAreas()) {
            workAreas.get(area.getValue()).setChecked(true);
        }
    }

    private boolean checkFields() {
        boolean catsReady = false;
        boolean areasReady = false;
        for (CheckBox chb : categories) {
            if (chb.isChecked())
                catsReady = true;
        }
        for (CheckBox chb : workAreas) {
            if (chb.isChecked())
                areasReady = true;
        }
        return catsReady && areasReady;
    }
}
