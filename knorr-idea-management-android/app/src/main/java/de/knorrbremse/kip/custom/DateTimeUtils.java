package de.knorrbremse.kip.custom;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class DateTimeUtils {

    private static final SimpleDateFormat IDEA_DATE_FORMAT;

    static {
        IDEA_DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);
    }

    public static String getIdeadateFormat(long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.setTimeInMillis(millis);
        return IDEA_DATE_FORMAT.format(calendar.getTime());
    }
}
