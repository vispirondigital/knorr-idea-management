package de.knorrbremse.kip.ui.custom;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.view.ViewGroup;
import android.widget.TextView;

import de.knorrbremse.kip.R;

public class SnackbarHelper {

    private static void configSnackbar(Context context, Snackbar snack) {
        addMargins(snack);
        setRoundBordersBg(context, snack);
        changeLineNumbers(snack);
        ViewCompat.setElevation(snack.getView(), 6f);
    }

    private static void addMargins(Snackbar snack) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) snack.getView().getLayoutParams();
        params.setMargins(0, 0, 0, 16);
        snack.getView().setLayoutParams(params);
    }

    private static void setRoundBordersBg(Context context, Snackbar snackbar) {
        snackbar.getView().setBackground(context.getDrawable(R.drawable.bg_snackbar));
    }

    private static void changeLineNumbers(Snackbar snackbar) {
        TextView tv = snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        tv.setMaxLines(5);
    }

    public static void createFloatingSnackbar(Context context, CoordinatorLayout clSnackbar) {
        String errorString = context.getString(R.string.no_internet_error_title) + "\n" + context.getString(R.string.no_internet_eror_text);
        createFloatingSnackbar(context,clSnackbar,errorString);
    }

    public static void createFloatingSnackbar(Context context, CoordinatorLayout clSnackbar, String errorString){
        Snackbar snackbar = Snackbar.make(clSnackbar, errorString, Snackbar.LENGTH_LONG);
        configSnackbar(context,snackbar);
        snackbar.show();
    }
}
