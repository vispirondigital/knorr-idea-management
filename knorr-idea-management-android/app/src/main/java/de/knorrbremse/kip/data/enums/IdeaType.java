package de.knorrbremse.kip.data.enums;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

public enum IdeaType implements Parcelable {

    SINGLE(0),
    GROUP(1);

    private static Map<Integer, IdeaType> types = new HashMap<>();

    static {
        for (IdeaType type : values()) {
            types.put(type.ideaType, type);
        }
    }

    private int ideaType;

    IdeaType(int type) {
        this.ideaType = type;
    }

    IdeaType(Parcel in) {
        ideaType = in.readInt();
    }

    public int getIdeaType() {
        return ideaType;
    }

    public static final Creator<IdeaType> CREATOR = new Creator<IdeaType>() {
        @Override
        public IdeaType createFromParcel(Parcel in) {
            return IdeaType.fromInt(in.readInt());
        }

        @Override
        public IdeaType[] newArray(int size) {
            return new IdeaType[size];
        }
    };

    public static IdeaType fromInt(int value) {
        return types.get(value);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(ideaType);
    }
}
