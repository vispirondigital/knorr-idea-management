package de.knorrbremse.kip.ui.ideas;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.truizlop.sectionedrecyclerview.SectionedRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import de.knorrbremse.kip.R;
import de.knorrbremse.kip.data.Idea;
import de.knorrbremse.kip.data.IdeaResponse;
import de.knorrbremse.kip.data.enums.ActualPhase;
import timber.log.Timber;

public class IdeasAdapter extends SectionedRecyclerViewAdapter<IdeaTitleViewHolder, IdeaViewHolder, FooterViewHolder> {

    private Context context;
    private ArrayList<IdeaResponse> inTesting;
    private ArrayList<IdeaResponse> inProgress;
    private ArrayList<IdeaResponse> inReview;
    private ArrayList<IdeaResponse> commited;
    private IdeaClickListener listener;

    public void setListener(IdeaClickListener listener) {
        this.listener = listener;
    }

    public IdeasAdapter(Context context) {
        this.context = context;
        inTesting = new ArrayList<>();
        inProgress = new ArrayList<>();
        inReview = new ArrayList<>();
        commited = new ArrayList<>();
    }

    public void setData(List<IdeaResponse> data) {
        inTesting.clear();
        inProgress.clear();
        inReview.clear();
        commited.clear();

        for (IdeaResponse idea : data) {
            Timber.d("Section: " + idea.getActualPhase() + ", Status: " + idea.getStatus());
            switch (idea.getActualPhase()) {
                case IN_TEST:
                    inTesting.add(idea);
                    break;
                case IN_REVIEW:
                    inReview.add(idea);
                    break;
                case IN_PROGRESS:
                    inProgress.add(idea);
                    break;
                case COMMITED:
                    commited.add(idea);
                    break;
            }
        }
        Timber.d("Sizes: In Testing: " + inTesting.size() + ", In Review: " + inReview.size()
                + ", In Progress: " + inProgress.size() + ", Commited: " + commited.size());
        notifyDataSetChanged();
    }

    @Override
    protected int getSectionCount() {
        return 4;
    }

    @Override
    protected int getItemCountForSection(int section) {
        switch (ActualPhase.fromInt(section)) {
            case IN_TEST:
                return inTesting.size();
            case IN_PROGRESS:
                return inProgress.size();
            case IN_REVIEW:
                return inReview.size();
            case COMMITED:
                return commited.size();
        }
        return 0;
    }

    @Override
    protected boolean hasFooterInSection(int section) {
        return false;
    }

    @Override
    protected IdeaTitleViewHolder onCreateSectionHeaderViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_idea_title, parent, false);
        return new IdeaTitleViewHolder(view);
    }

    @Override
    protected FooterViewHolder onCreateSectionFooterViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected IdeaViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_idea, parent, false);
        return new IdeaViewHolder(view);
    }

    @Override
    protected void onBindSectionHeaderViewHolder(IdeaTitleViewHolder holder, int section) {
        switch (ActualPhase.fromInt(section)) {
            case IN_TEST:
                holder.bindData(context.getString(R.string.testphase));
                break;
            case IN_PROGRESS:
                holder.bindData(context.getString(R.string.implemented));
                break;
            case IN_REVIEW:
                holder.bindData(context.getString(R.string.examination));
                break;
            case COMMITED:
                holder.bindData(context.getString(R.string.received));
                break;
        }
    }

    @Override
    protected void onBindSectionFooterViewHolder(FooterViewHolder holder, int section) {

    }

    @Override
    protected void onBindItemViewHolder(IdeaViewHolder holder, int section, int position) {
        switch (ActualPhase.fromInt(section)) {
            case IN_TEST:
                holder.bindData(inTesting.get(position));
                break;
            case IN_PROGRESS:
                holder.bindData(inProgress.get(position));
                break;
            case IN_REVIEW:
                holder.bindData(inReview.get(position));
                break;
            case COMMITED:
                holder.bindData(commited.get(position));
                break;
        }

        if (listener != null) {
            bindListener(holder, section, position);
        }
    }

    private void bindListener(IdeaViewHolder holder, int section, int position) {
        switch (ActualPhase.fromInt(section)) {
            case IN_TEST:
                holder.itemView.setOnClickListener(click -> listener.onIdeaClick(inTesting.get(position)));
                break;
            case IN_PROGRESS:
                holder.itemView.setOnClickListener(click -> listener.onIdeaClick(inProgress.get(position)));
                break;
            case IN_REVIEW:
                holder.itemView.setOnClickListener(click -> listener.onIdeaClick(inReview.get(position)));
                break;
            case COMMITED:
                holder.itemView.setOnClickListener(click -> listener.onIdeaClick(commited.get(position)));
                break;
        }
    }

    interface IdeaClickListener {
        void onIdeaClick(IdeaResponse idea);
    }
}
