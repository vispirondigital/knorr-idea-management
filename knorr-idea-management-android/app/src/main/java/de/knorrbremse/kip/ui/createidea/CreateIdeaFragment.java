package de.knorrbremse.kip.ui.createidea;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;

import androidx.navigation.Navigation;
import butterknife.BindColor;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import de.knorrbremse.kip.MainActivity;
import de.knorrbremse.kip.R;
import de.knorrbremse.kip.data.Cocreator;
import de.knorrbremse.kip.data.CreateIdeaRequest;
import de.knorrbremse.kip.data.enums.Category;
import de.knorrbremse.kip.data.enums.IdeaType;
import de.knorrbremse.kip.data.enums.WorkArea;
import de.knorrbremse.kip.ui.BaseViewModelFragment;
import de.knorrbremse.kip.ui.custom.InputErrorLayout;
import de.knorrbremse.kip.ui.custom.SnackbarHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateIdeaFragment extends BaseViewModelFragment<CreateIdeaViewModel> {

    @BindView(R.id.til_idea_desc)
    TextInputLayout tilIdeaDesc;

    @BindView(R.id.et_idea_desc)
    EditText etIdeaDesc;

    @BindView(R.id.v_desc_error_line)
    View vDescErrorLine;

    @BindView(R.id.iv_desc_error)
    ImageView ivDescError;

    @BindView(R.id.tv_categories_error)
    TextView tvCategoriesError;

    @BindView(R.id.iv_categories_error)
    ImageView ivCategoriesError;

    @OnClick(R.id.btn_save_idea)
    public void createIdea() {
        if (checkIdea()) {
            activity.showLoading(true);
            viewModel.createIdea(idea);
        }
    }

    @OnClick(R.id.ib_arrow)
    public void toCategories() {
        getDataFromFields();
        Bundle bundle = new Bundle();
        bundle.putParcelable("idea", idea);
        Navigation.findNavController(getView()).navigate(R.id.action_createIdeaFragment_to_categoriesAreasFragment, bundle);
    }

    @OnClick(R.id.rb_group)
    public void toGroup() {
        getDataFromFields();
        Bundle bundle = new Bundle();
        bundle.putParcelable("idea", idea);
        Navigation.findNavController(getView()).navigate(R.id.action_createIdeaFragment_to_groupFragment, bundle);
        if (idea.getIdeaType() == IdeaType.SINGLE)
            rgIdeaType.check(R.id.rb_single);
    }

    @OnFocusChange(R.id.et_idea_desc)
    public void etIdeaDescFocused(boolean focused) {
        if (focused) {
            navigateToIdeaDec();
        }
    }

    @OnClick(R.id.rb_single)
    public void onSingleSelected() {
        idea.getCocreators().clear();
    }

    @BindView(R.id.iel_idea_name)
    InputErrorLayout ielIdeaName;

    @BindView(R.id.rg_idea_type)
    RadioGroup rgIdeaType;

    @BindView(R.id.rb_single)
    RadioButton rbSingle;

    @BindView(R.id.tv_idea_desc_error)
    TextView tvError;

    @BindView(R.id.et_categories)
    TextView etCategories;

    @BindView(R.id.cl_snackbar)
    CoordinatorLayout clSnackbar;

    @BindColor(R.color.red)
    int red;

    @OnClick(R.id.til_idea_desc)
    public void toIdeaDesc() {
        navigateToIdeaDec();
    }

    @OnClick(R.id.et_idea_desc)
    public void toIdeaDescEt() {
        navigateToIdeaDec();
    }

    private CreateIdeaRequest idea;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_create_idea;
    }

    @Override
    public void createViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CreateIdeaViewModel.class);
    }

    @Override
    public void subscribe() {
        viewModel.getIdeaCreated().observe(this, response -> {
            activity.showLoading(false);
            Navigation.findNavController(getView()).popBackStack();
        });

        viewModel.getOnError().observe(this, error -> {
            activity.showLoading(false);
            SnackbarHelper.createFloatingSnackbar(getContext(), clSnackbar);
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        MainActivity activity = (MainActivity) getActivity();
        assert activity != null;
        activity.setupToolbar(getString(R.string.new_idea), false, false);
        activity.showToolbar(true);
        activity.hideReward();
        final InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        CreateIdeaRequest fromArgs = (CreateIdeaRequest) getArguments().get("idea");
        if (idea == null) {
            if (fromArgs == null) {
                rgIdeaType.check(R.id.rb_single);
                createEmptyIdea();
            } else {
                idea = fromArgs;
                fillIn();
            }
        } else {
            fillIn();
        }
        setTextWatchers();
    }

    @Override
    public void onResume() {
        super.onResume();
        tvCategoriesError.requestFocus();
        ielIdeaName.getEt().clearFocus();
        ielIdeaName.getEt().setSelected(false);
    }

    private void fillIn() {
        rgIdeaType.clearCheck();
        rgIdeaType.check(idea.getIdeaType() == IdeaType.GROUP ? R.id.rb_group : R.id.rb_single);
        ielIdeaName.prefill(idea.getName());
        etIdeaDesc.setText(idea.getDescription());
        if (idea.getCategories().size() > 0) {
            StringBuilder builder = new StringBuilder();
            for (Category category : idea.getCategories()) {
                builder.append(getString(category.getString())).append(",");
            }
            builder.setCharAt(builder.length() - 1, ';');
            for (WorkArea area : idea.getWorkingAreas()) {
                builder.append(getString(area.getString())).append(',');
            }
            builder.deleteCharAt(builder.length() - 1);
            etCategories.setText(builder.toString());
        }
    }

    private void getDataFromFields() {
        idea.setName(TextUtils.isEmpty(ielIdeaName.getText()) ? "" : ielIdeaName.getText());
        Iterator<Cocreator> i = idea.getCocreators().iterator();
        while (i.hasNext()) {
            Cocreator cocreator = i.next();
            if (TextUtils.isEmpty(cocreator.getPersonalId()))
                i.remove();
        }
    }

    private void navigateToIdeaDec() {
        getDataFromFields();
        Bundle bundle = new Bundle();
        bundle.putParcelable("idea", idea);
        Navigation.findNavController(getView()).navigate(R.id.action_createIdeaFragment_to_ideaDescFragment, bundle);
    }

    private void createEmptyIdea() {
        idea = new CreateIdeaRequest(new ArrayList<>(), new ArrayList<>(),
                etIdeaDesc.getText().toString(), ielIdeaName.getText(),
                IdeaType.SINGLE, new ArrayList<>());
    }

    private void setTextWatchers() {
        ielIdeaName.setTextWatcher(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (ielIdeaName.hasError() && ielIdeaName.getText().length() > 0) {
                    ielIdeaName.hideError();
                }
            }
        });

        etIdeaDesc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (tvError.getVisibility() == View.VISIBLE && etIdeaDesc.getText().length() > 0) {
                    tvError.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    public boolean checkIdea() {
        boolean ready = true;
        if (TextUtils.isEmpty(ielIdeaName.getText())) {
            ready = false;
            ielIdeaName.showError();
        }
        if (TextUtils.isEmpty(etIdeaDesc.getText())) {
            ready = false;
            tvError.setVisibility(View.VISIBLE);
            vDescErrorLine.setVisibility(View.VISIBLE);
            ivDescError.setVisibility(View.VISIBLE);
            tilIdeaDesc.setDefaultHintTextColor(createErrorHintColors());
        }
        if (idea.getCategories().size() == 0) {
            ready = false;
            etCategories.setBackgroundResource(R.drawable.red_border_rounded_left_bcg);
            etCategories.setHintTextColor(red);
            tvCategoriesError.setVisibility(View.VISIBLE);
            ivCategoriesError.setVisibility(View.VISIBLE);
        }
        return ready;
    }

    private ColorStateList createErrorHintColors() {
        int[][] states = new int[][]{
                new int[]{android.R.attr.state_enabled}, // enabled
                new int[]{-android.R.attr.state_enabled}, // disabled
                new int[]{-android.R.attr.state_checked}, // unchecked
                new int[]{android.R.attr.state_pressed}  // pressed
        };

        int[] colorsError = new int[]{
                getResources().getColor(R.color.red),
                getResources().getColor(R.color.red),
                getResources().getColor(R.color.red),
                getResources().getColor(R.color.red),
        };
        return new ColorStateList(states, colorsError);
    }
}
