package de.knorrbremse.kip.ui.agb;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.knorrbremse.kip.R;
import de.knorrbremse.kip.ui.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class AGBFragment extends BaseFragment {

    @Override
    public int getLayoutId() {
        return R.layout.fragment_agb;
    }

}
