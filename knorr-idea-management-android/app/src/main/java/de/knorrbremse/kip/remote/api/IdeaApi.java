package de.knorrbremse.kip.remote.api;

import de.knorrbremse.kip.data.CreateIdeaRequest;
import de.knorrbremse.kip.data.CreateIdeaResponse;
import de.knorrbremse.kip.data.EmployeesLambdaResponse;
import de.knorrbremse.kip.data.IdeasLambdaResponse;
import io.reactivex.Single;

public interface IdeaApi {

    Single<IdeasLambdaResponse> getIdeas();

    Single<CreateIdeaResponse> createIdea(CreateIdeaRequest idea);

    Single<EmployeesLambdaResponse> getEmployees();
}
