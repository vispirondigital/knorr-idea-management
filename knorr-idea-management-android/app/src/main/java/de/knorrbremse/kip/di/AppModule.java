package de.knorrbremse.kip.di;

import android.content.Context;

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;
import com.amazonaws.regions.Regions;

import dagger.Module;
import dagger.Provides;
import de.knorrbremse.kip.KipApp;
import de.knorrbremse.kip.custom.KeyHelper;
import de.knorrbremse.kip.data.Constants;
import de.knorrbremse.kip.db.KipDatabase;
import de.knorrbremse.kip.db.RoomDbProvider;
import de.knorrbremse.kip.remote.TokenProvider;

@Module(includes = {ViewModelModule.class})
public class AppModule {

    @ApplicationScope
    @Provides
    public CognitoUserPool provideCognitoUserPool(Context context) {
        return new CognitoUserPool(context,
                Constants.COGNITO_USER_POOL_ID,
                Constants.COGNITO_CLIENT_ID,
                Constants.COGNITO_CLIENT_SECRET,
                Regions.EU_CENTRAL_1);
    }

    @ApplicationScope
    @Provides
    public TokenProvider provideTokenProvider(Context context, KeyHelper keyHelper, CognitoUserPool userPool) {
        return new TokenProvider(context, keyHelper, userPool);
    }

    @ApplicationScope
    @Provides
    public KeyHelper provideKeyHelper(Context context) {
        return KeyHelper.getInstance(context);
    }

    @ApplicationScope
    @Provides
    public KipDatabase provideRoom(KipApp app) {
        return KipDatabase.getInstance(app);
    }


    @Provides
    @ApplicationScope
    public RoomDbProvider provideRoomDbProvider(KipApp app) {
        return new RoomDbProvider(KipDatabase.getInstance(app));
    }
}
