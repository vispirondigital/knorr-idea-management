package de.knorrbremse.kip.ui.ideas;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.navigation.Navigation;
import butterknife.BindView;
import butterknife.OnClick;
import de.knorrbremse.kip.MainActivity;
import de.knorrbremse.kip.R;
import de.knorrbremse.kip.data.IdeaResponse;
import de.knorrbremse.kip.ui.BaseViewModelFragment;
import de.knorrbremse.kip.ui.custom.SnackbarHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class IdeasFragment extends BaseViewModelFragment<IdeasViewModel> implements IdeasAdapter.IdeaClickListener {

    @BindView(R.id.rv_ideas)
    RecyclerView rvIdeas;

    @BindView(R.id.cl_empty)
    ConstraintLayout clEmpty;

    @BindView(R.id.cl_list)
    ConstraintLayout clList;

    @BindView(R.id.cl_snackbar)
    CoordinatorLayout clSnackbar;

    @BindView(R.id.tv_welcome_title)
    TextView tvWelcomeTitle;

    @BindView(R.id.btn_new_idea)
    Button btnNewIdea;

    @OnClick(R.id.btn_new_idea)
    public void newIdea() {
        Navigation.findNavController(getView()).navigate(R.id.action_ideasFragment_to_createIdeaFragment);
    }

    public IdeasFragment() {
        // Required empty public constructor
    }

    private IdeasAdapter ideasAdapter;
    private boolean seenWelcome;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_ideas;
    }

    @Override
    public void createViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(IdeasViewModel.class);
    }

    @Override
    public void subscribe() {
        viewModel.getLoggedOut().observe(this, loggedOut -> {
            if (loggedOut) {
                activity.showLoading(false);
                Navigation.findNavController(getView()).navigate(IdeasFragmentDirections.actionIdeasFragmentToLoginFragment());
            }
        });
        viewModel.getIdeas().observe(this, ideas -> {
            activity.showLoading(false);
            if (ideas!= null && ideas.size() > 0) {
                clList.setVisibility(View.VISIBLE);
                ideasAdapter.setData(ideas);
            } else {
                if (seenWelcome) {
                    btnNewIdea.setText(R.string.new_idea);
                    clList.setVisibility(View.VISIBLE);
                } else
                    viewModel.getUserName();
            }
        });
        viewModel.getOnError().observe(this, error -> {
            activity.showLoading(false);
            clList.setVisibility(View.VISIBLE);
            SnackbarHelper.createFloatingSnackbar(getContext(), clSnackbar);
        });

        viewModel.getEmail().observe(this, email -> {
            String[] parts = email.split("\\.");
            String name = parts[0];
            String caps = name.substring(0, 1).toUpperCase() + name.substring(1);
            tvWelcomeTitle.setText(getString(R.string.hallo, caps));
            clEmpty.setVisibility(View.VISIBLE);
            btnNewIdea.setText(R.string.lets_start);
            seenWelcome = true;
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (ideasAdapter == null)
            ideasAdapter = new IdeasAdapter(getContext());
        rvIdeas.setAdapter(ideasAdapter);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        rvIdeas.setLayoutManager(manager);
        ideasAdapter.setListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        assert activity != null;
        activity.setupToolbar(getString(R.string.my_ideas), false, true);
        activity.showToolbar(true);
        activity.hideReward();
        activity.getLogout().setOnClickListener(click -> {
            activity.showLoading(true);
            viewModel.logOut();
        });
        viewModel.refresh();
        activity.showLoading(true);
    }

    @Override
    public void onIdeaClick(IdeaResponse idea) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("idea", idea);
        Navigation.findNavController(getView()).navigate(R.id.action_ideasFragment_to_ideaDetailFragment, bundle);
    }
}
