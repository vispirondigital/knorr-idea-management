package de.knorrbremse.kip.data;

import com.google.gson.annotations.SerializedName;

public class CreateIdeaResponse {

    @SerializedName("statusCode")
    public int statusCode;
}
