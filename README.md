# Knorr idea management
---
# knorr-idea-management-backend
# Configuration and deploy to AWS
Backend for configuration and deploy on AWS cloud. Using the features like API Gateway, Cognito, Lambda function, DynamoDB, S3, Cloudformation.
These few steps will provide you by how to configure your AWS account, set up your environment and run the deploy configuration.  

##1. Create and set up account on AWS
###1.1 Create Account on AWS
To use and set up AWS you need an AWS account. 
For registration new user you have to set up an email and valid credit card.
 
see [How to create and activate AWS account](https://aws.amazon.com/premiumsupport/knowledge-center/create-and-activate-aws-account/)
    

###1.2 Create USER on IAM for deploying

To use AWS CLI on your local environment you will need IAM user, which provides credential for your local environment.

see [documentation](https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/signup-create-iam-user.html)

Set IAM user and attach policies (needed during development):
* AWSLambdaFullAccess
* IAMFullAccess
* AmazonS3FullAccess
* CloudFrontFullAccess,
* AWSCodeDeployRole
* AmazonAPIGatewayAdministrator
* AWSCodeDeployRoleForLambda
* AmazonCognitoPowerUser*
* AWSCodeDeployDeployerAccess
* CloudFormation_FullAccess
    * This is a custom policy.
    * Create it manually in the IAM Console using file CloudFormation_FullAccess.json

###1.2 Create S3 bucket
Modules for serverless proccessing (cloudformation) are uploaded and stored in S3.

For this purpose we need to create S3 bucket
(e.g. with name 'idea-serverless-bucket', used in 2.3.1 step ).
Remember that S3 Bucket names are UNIQUE across AWS regions (the name could be taken!)

see [documentation](https://docs.aws.amazon.com/quickstarts/latest/s3backup/step-1-create-bucket.html)

##2 Set up you local environment for development and deployment to AWS (Cloudformation)
###2.1 Set up AWS credentials in to your environment
To connect to AWS services you need to set up credential to you local environment

Set credentials in the AWS credentials profile file on your local system, located at:



C:\Users\USERNAME\\.aws\credentials on Windows

/home/USER_dir/.aws/credentials on unix

This file should contain lines in the following format:

        [default]
        aws_access_key_id = your_access_key_id
        aws_secret_access_key = your_secret_access_key
        Substitute your own AWS credentials values for the values your_access_key_id and your_secret_access_key.

Set the AWS Region in the AWS config file on your local system, located at:



C:\Users\USERNAME\\.aws\config on Windows

/home/USER_dir/.aws/config on unix

This file should contain lines in the following format:

        [default]
        region = eu-central-1
 
for more see [Setup Credentials](https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/setup-credentials.html)
###2.2 Download and install Java8, Maven, AWS CLI, (AWS Toolkit for Eclipse )
For run and deploy serverless configuration to AWS S3/Cloudformation you need a properly configured JAVA environment with MAVEN tool and  you need to download and install [AWS CLI](https://aws.amazon.com/cli/).
AWS CLI is AWS Command Line Interface (CLI) is a unified tool to manage your AWS services. 

####2.3 If using Eclipse...
For developing in eclipse could by useful [AWS toolkit](https://docs.aws.amazon.com/toolkit-for-eclipse/v1/user-guide/setup-install.html) which provide several features for development on AWS infrastructure services in Eclipse, including Amazon S3, Amazon EC2, and Amazon DynamoDB. 


###2.4 Configuration
When you have maven and aws cli installed,
you are able to run configuration but before please
check and update the following parameters in your deployment environment
(you can also use appropriate flags when running the deployment script)

        export CLOUD_COMPANY="Knorr-Bremse"
        export CLOUD_APPLICATION=weCon
        export CLOUD_S3_BUCKET='The name of the bucket you created'
        

###2.5 Deployment
####2.5.1 Backend      
Use the command-line environment and the deploy python script:

        cd knorr-idea-management-backend

        ./deploy.py --help        
        ./deploy.py --environment LIVE --appVersion 1.0.0


####2.5.2 Mobile front-end
The deployment script (deploy.py), when successful, it will print
in the terminal configuration options.
      
Use the the printed "User Pool ID", "Android Client ID" and "Android Client Secret"
to configure and build the android app.

####2.5.3 Web-admin page
The web ADMIN page can be found in the S3 bucket

    s3_bucket/environment/get-idea-list.html

## 3. Post deploy configuration 
###3.1 Configure DynamoDB
SignUp is based on verification of email and personalId of employee.

This records are stored in (environment-)UsersForApplication (dynamoDB table, created in 2.3.1 step) in next format:

        {
            "personalId": "personal_id",
            "email": "email@email.de",
            "firstName": "first_name",
            "lastName": "last_name",
            "isUser": true,
            "isAdmin": true,
        }
        
isUser flag true marks an mobile app users, false are for employees with access denied to mobile app.
isAdmin flag true marks an admin web app users, false are for mobile users or employees with access denied to mobile app.
Please set up the employee list to this table in correct format.  

###3.2 Configure verification message
There is send verification email to the signUped user.
The custom for must be set manually AWS console.

'AWS services' -> 'Cognito' -> 'Manage User Pools' -> Choose the created user pool e.g.:'IdeaUserPool'

-> 'General settings' ->  'Message customizations'

Switch 'Verification type' to link
Set up the 'Email subject': 

        weCon verification link

Set up the 'Email message' [(for example, see image doc/cognito_message.png)](./knorr-idea-management-backend/doc/cognito_message.png)


    Sehr geehrte Damen und Herren,
    <br/>
    <br/>
    vielen Dank für die Anmeldung bei weCon.
    <br/>
    Um Ihren Account zu aktivieren, müssen Sie {##hier##} Ihre E Mail Adresse bestätigen.
    <br/>
    <br/>
    Viel Spaß und erfolgreiche Ideen bei der Nutzung von weCon.
    <br/>
    Ihr Knorr-Bremse Team

and scroll down and click

        Save changes
        

# That's it! You are now deployed :-)        

        

