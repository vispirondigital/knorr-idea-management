import json
from argparse import Namespace

from deploy_helper.runner import run_command


def sanitize(tuple_or_string: (tuple, str)) -> str:
    """
    Some JSON output from AWS CLI commands gets json.loads(..) into a tuple with only one element.
    :return: If the input is a tuple, return the first element. Otherwise, return the original value
    """

    return tuple_or_string[0] if isinstance(tuple_or_string, tuple) else tuple_or_string


def get_user_pool(args: Namespace) -> dict:
    if args.dry_run:
        return {"Id": "dry_run_CognitoUserPool_id"}

    user_pools = json.loads(run_command(
        ["aws", "cognito-idp", "list-user-pools", "--max-results=10"], args, return_stdout=True)
    )['UserPools']
    for pool in user_pools:
        # print("pool : %s" % pool)
        if pool['Name'] == "%s-IdeaUserPool" % args.environment:
            return pool

    print(repr(user_pools))
    raise RuntimeError("Could not find Cognito User pool")


def get_rest_api(args: Namespace) -> dict:
    if args.dry_run:
        return {"id": "dry_run_APIGATEWAY_id"}

    rest_apis = json.loads(run_command(["aws", "apigateway", "get-rest-apis"], args, return_stdout=True))['items']
    for api in rest_apis:
        # print("api : %s" % api)
        if api['name'] == "%s - Ideas API" % args.environment:
            return api

    print(repr(rest_apis))
    raise RuntimeError("Could not find REST API Gateway")


def get_existing_user_pools(args: Namespace, user_pool_id: str) -> list:
    if args.dry_run:
        return []

    return json.loads(run_command(
        ["aws", "cognito-idp", "list-user-pool-clients",
         "--user-pool-id", user_pool_id,
         "--max-results=10"], args, return_stdout=True)
    )['UserPoolClients']


def ensure_client(args: Namespace,
                  client_name: str, auth_flow: str, generate_secret: str,
                  existing_user_pools: list, user_pool_id: str) -> str:
    """
    Check whether a pool with the given name already exists. If not, create it

    :return: the Client ID
    """
    if args.dry_run:
        return "dry_run_" + client_name

    for client in existing_user_pools:
        if client["ClientName"] == client_name:
            return sanitize(client["ClientId"])

    pool_create_command = ["aws", "cognito-idp", "create-user-pool-client",
                           "--user-pool-id", user_pool_id,
                           "--client-name", client_name,
                           "--refresh-token-validity", "30",
                           "--explicit-auth-flows", auth_flow,
                           ]
    if generate_secret:
        pool_create_command.append("--generate-secret")
    else:
        pool_create_command.append("--no-generate-secret")

    output = run_command(pool_create_command, args, return_stdout=True)

    # Return the CLIENTID for this client
    return sanitize(json.loads(output)["UserPoolClient"]["ClientId"])


def get_user_pool_client_secret(args: Namespace, client_id: str, user_pool_id: str) -> str:
    if args.dry_run:
        return "dry_run_client_secret"

    pool_describe_command = ["aws", "cognito-idp", "describe-user-pool-client",
                             "--user-pool-id", user_pool_id,
                             "--client-id", client_id,
                             ]
    output = run_command(pool_describe_command, args, return_stdout=True)

    client = json.loads(output)["UserPoolClient"]
    if "ClientSecret" in client:
        return client["ClientSecret"]
    else:
        return None


def get_cognito_domain(args: Namespace, cognito_id: str) -> str:
    if args.dry_run:
        return "dry_run_Cognito_domain"

    get_api_create_command = ["aws", "cognito-idp", "describe-user-pool", "--user-pool-id", "%s" % cognito_id]
    output = run_command(get_api_create_command, args, return_stdout=True)

    user_pool_descr = json.loads(output)["UserPool"]
    if "Domain" in user_pool_descr:
        return user_pool_descr["Domain"]
    else:
        return ""


def get_id_from_run_create_deployment(args: Namespace, rest_id: str, env_stage: str) -> str:
    if args.dry_run:
        return "dry_run_API_Gateway_deployment_ID"

    get_api_create_command = [
        "aws", "apigateway", "create-deployment",
        "--rest-api-id", "%s" % rest_id,
        "--stage-name", "%s" % env_stage]
    output = run_command(get_api_create_command, args, return_stdout=True)

    return json.loads(output)["id"]


def convert_template(filename: str, new_name: str, stack_runtime_parameters: dict):
    with open(filename) as template_file:
        content = template_file.read()
        for var in stack_runtime_parameters:
            content = content.replace("$%s$" % var, stack_runtime_parameters[var])
        with open(new_name, "w") as new_file:
            new_file.write(content)


def create_move_to_s3_file_command(args: Namespace, filename: str, basepath="../knorr-idea-management-web/") -> list:
    return ["aws", "s3", "cp",
            basepath + filename,
            "s3://%s/%s-page/%s" % (args.s3_bucket, args.environment, filename),
            "--acl", "public-read",
            ]


def create_tag_s3_file_command(args: Namespace, filename: str, tags: dict) -> list:
    tag_set = ["{Key=%s,Value=%s}" % (key, tags[key]) for key in tags]
    return ["aws", "s3api", "put-object-tagging",
            "--bucket", args.s3_bucket,
            "--key", "%s-page/%s" % (args.environment, filename),
            "--tagging", "TagSet=[%s]" % ','.join(tag_set)
            ]
