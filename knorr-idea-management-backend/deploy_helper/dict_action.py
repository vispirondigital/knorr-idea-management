import argparse


class DictAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        tag = getattr(namespace, self.dest)
        if tag is None:
            tag = {}
            setattr(namespace, self.dest, tag)
        s = values.split('=')
        if len(s) < 2 or len(s[0].strip()) == 0:
            raise argparse.ArgumentTypeError("tags must be in form key=value")
        tag[s[0]] = '='.join(s[1:])