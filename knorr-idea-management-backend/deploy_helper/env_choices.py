class EnvChoices(object):
    def __contains__(self, item):
        if item in ["LIVE", "STAGE"]:
            return True
        return item.startswith("DEV") or item.startswith("TEST")

    # useful when the argparser complains for invalid choice
    def __iter__(self):
        return iter(["LIVE", "STAGE", "TEST*", "DEV*"])