import os

from deploy_helper.runner import get_stdout


def getenv(env_key: str) -> str:
    if env_key in os.environ:
        return os.environ[env_key].strip()
    else:
        return None


def get_commit_hash(git_bin="/usr/bin/git") -> str:
    commit_hash = get_stdout([git_bin, "rev-parse", "HEAD"])
    # mods = get_stdout([git_bin, "status", "--untracked-files=no", "-s"])

    if len(commit_hash) == 0:
        return "(no GIT repo found)"

    return commit_hash


# https://stackoverflow.com/questions/3041986/apt-command-line-interface-like-yes-no-input
def query_yes_no(question: str, default="yes") -> bool:
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        choice = input(question + prompt).lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            print("Please respond with 'yes' or 'no' (or 'y' or 'n').\n")