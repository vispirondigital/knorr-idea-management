import subprocess
from argparse import Namespace


def run_command(command_list: list, args: Namespace, return_stdout=False):
    """
    Print and then run the command (if --dry-run is not set) making sure
    to exit() in case it returns non-zero exit code so that
    further commands do not run
    """
    print(" ".join(command_list))
    if not args.dry_run:
        completed_process = subprocess.run(command_list, stdout=(subprocess.PIPE if return_stdout else None))

        out = completed_process.stdout.decode('utf-8').strip() if return_stdout else None
        if out:
            print(out)

        if completed_process.returncode != 0:
            print("Command finished with non-zero return code: " + repr(completed_process))
            exit(completed_process.returncode)

        return out


def get_stdout(command_list: list) -> str:
    return subprocess.run(
        command_list,
        stdout=subprocess.PIPE, stderr=subprocess.PIPE
    ).stdout.decode('utf-8').strip()
