package de.vispron.idea.lambda.idea;

import java.util.ArrayList;
import java.util.Date;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.util.StringUtils;

import de.vispron.idea.lambda.idea.model.IdeaDetail;
import de.vispron.idea.lambda.idea.model.IdeaLog;
import de.vispron.idea.lambda.idea.model.Response;

/**
 * Provides idea creation service.
 */
public class CreateIdea implements RequestHandler<IdeaDetail, Response> {

    public static final String NO_NAME = "No name for idea has been set.";

    public static final String NO_DESCRIPTION = "No description for idea has been set.";

    public static final String NO_CREATED_BY = "No createBy for idea has been set.";

    public static final String NO_CREATED_BY_COGNITO = "No createCognitoId for idea has been set.";

    public static final String NO_TYPE = "No ideaType for idea has been set.";

    public static final String NO_CATEGORY = "No category for idea has been set.";

    public static final String NO_WORKING = "No workingArea for idea has been set.";

    public static final String IDEA_OPEN = "OPEN";

    public static final String IDEA_COMMITED = "COMMITED";

    @Override
    public Response handleRequest(IdeaDetail input, Context context) {
        try {
            context.getLogger().log("Create idea " + input.getName() + " by " + input.getCreatedEmailBy());

            if (StringUtils.isNullOrEmpty(input.getName())) {
                return new Response(400, NO_NAME);
            }

            if (StringUtils.isNullOrEmpty(input.getCreatedEmailBy())) {
                return new Response(400, NO_CREATED_BY);
            }

            if (StringUtils.isNullOrEmpty(input.getCreatedCognitoIdBy())) {
                return new Response(400, NO_CREATED_BY_COGNITO);
            }

            if (StringUtils.isNullOrEmpty(input.getDescription())) {
                return new Response(400, NO_DESCRIPTION);
            }

            if (StringUtils.isNullOrEmpty(input.getIdeaType())) {
                return new Response(400, NO_TYPE);
            }

            if (input.getCategory() == null || input.getCategory().isEmpty()) {
                return new Response(400, NO_CATEGORY);
            }

            if (input.getWorkingArea() == null || input.getWorkingArea().isEmpty()) {
                return new Response(400, NO_WORKING);
            }

            Date createDate = new Date();

            input.setCreatedAt(createDate);
            input.setStatus(IDEA_OPEN);
            input.setImplementationStatus(IDEA_COMMITED);
            input.setLogs(new ArrayList<IdeaLog>());

            IdeaDetail.save(input);

            context.getLogger().log("Created idea.");

            return new Response(200, "ok");
        } catch (Throwable e) {
            return new Response(400, e);
        }
    }
}