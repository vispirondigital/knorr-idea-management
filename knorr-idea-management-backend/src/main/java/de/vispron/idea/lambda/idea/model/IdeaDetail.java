package de.vispron.idea.lambda.idea.model;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.TableNameOverride;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import de.vispron.idea.lambda.IdeaSettings;

/**
 * Entity for Idea, has annotation for json and dynamoDb ORM
 *
 */
@DynamoDBTable(tableName = "Ideas")
public class IdeaDetail {

    private static final String IDEAS_TABLE_NAME = System.getenv("IDEAS_TABLE_NAME");

    private static DynamoDBMapper mapper = new DynamoDBMapper(
            AmazonDynamoDBClientBuilder.standard().withRegion(IdeaSettings.getRegion()).build());

    public static List<IdeaDetail> getList(DynamoDBScanExpression dynamoDBScanExpression) {
        DynamoDBMapperConfig configs = new DynamoDBMapperConfig.Builder()
                .withTableNameOverride(TableNameOverride.withTableNameReplacement(IDEAS_TABLE_NAME)).build();
        return mapper.scan(IdeaDetail.class, dynamoDBScanExpression, configs);
    }

    public static IdeaDetail loadIdea(String cognitoId, Date createdAt) {
        DynamoDBMapperConfig configs = new DynamoDBMapperConfig.Builder()
                .withTableNameOverride(TableNameOverride.withTableNameReplacement(IDEAS_TABLE_NAME)).build();
        return mapper.load(IdeaDetail.class, cognitoId, createdAt, configs);
    }

    public static void save(IdeaDetail ideaDetail) {
        DynamoDBMapperConfig configs = new DynamoDBMapperConfig.Builder()
                .withTableNameOverride(TableNameOverride.withTableNameReplacement(IDEAS_TABLE_NAME)).build();
        mapper.save(ideaDetail, configs);
    }

    public IdeaDetail() {
    }

    private String createdEmailBy;

    private String createdCognitoIdBy;

    /**
     * DynamoDB stores date as String in format ISO 8601 2018-10-16
     * 2018-10-16T07:44:05+00:00 2018-10-16T07:44:05Z 20181016T074405Z
     * 
     */
    private Date createdAt;

    private String description;

    private String name;

    private String status;

    private String implementationStatus;

    private String ideaType;

    private List<String> category;

    private List<IdeaUser> cocreators;

    private List<String> workingArea;

    private String approverComment;

    private IdeaUser approvedBy;

    private Double bonus;

    private List<IdeaLog> logs;

    @XmlElement
    @DynamoDBAttribute(attributeName = "createdEmailBy")
    public String getCreatedEmailBy() {
        return createdEmailBy;
    }

    public void setCreatedEmailBy(String createdEmailBy) {
        this.createdEmailBy = createdEmailBy;
    }

    @XmlElement
    @DynamoDBRangeKey(attributeName = "createdAt")
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @XmlElement
    @DynamoDBHashKey(attributeName = "createdCognitoIdBy")
    public String getCreatedCognitoIdBy() {
        return createdCognitoIdBy;
    }

    public void setCreatedCognitoIdBy(String createdCognitoIdBy) {
        this.createdCognitoIdBy = createdCognitoIdBy;
    }

    @XmlElement
    @DynamoDBAttribute(attributeName = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlElement
    @DynamoDBAttribute(attributeName = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement
    @DynamoDBAttribute(attributeName = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @XmlElement
    @DynamoDBAttribute(attributeName = "implementationStatus")
    public String getImplementationStatus() {
        return implementationStatus;
    }

    public void setImplementationStatus(String implementationStatus) {
        this.implementationStatus = implementationStatus;
    }

    @XmlElement
    @DynamoDBAttribute(attributeName = "ideaType")
    public String getIdeaType() {
        return ideaType;
    }

    public void setIdeaType(String ideaType) {
        this.ideaType = ideaType;
    }

    @XmlElement
    @DynamoDBAttribute(attributeName = "category")
    public List<String> getCategory() {
        return category;
    }

    public void setCategory(List<String> category) {
        this.category = category;
    }

    @XmlElement
    @DynamoDBAttribute(attributeName = "cocreators")
    public List<IdeaUser> getCocreators() {
        return cocreators;
    }

    public void setCocreators(List<IdeaUser> cocreators) {
        this.cocreators = cocreators;
    }

    @XmlElement
    @DynamoDBAttribute(attributeName = "workingArea")
    public List<String> getWorkingArea() {
        return workingArea;
    }

    public void setWorkingArea(List<String> workingArea) {
        this.workingArea = workingArea;
    }

    @XmlElement
    @DynamoDBAttribute(attributeName = "approverComment")
    public String getApproverComment() {
        return approverComment;
    }

    public void setApproverComment(String approverComment) {
        this.approverComment = approverComment;
    }

    @XmlElement
    @DynamoDBAttribute(attributeName = "approvedBy")
    public IdeaUser getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(IdeaUser approvedBy) {
        this.approvedBy = approvedBy;
    }

    @XmlElement
    @DynamoDBAttribute(attributeName = "bonus")
    public Double getBonus() {
        return bonus;
    }

    public void setBonus(Double bonus) {
        this.bonus = bonus;
    }

    @XmlElement
    @DynamoDBAttribute(attributeName = "logs")
    public List<IdeaLog> getLogs() {
        return logs;
    }

    public void setLogs(List<IdeaLog> logs) {
        this.logs = logs;
    }
}
