package de.vispron.idea.lambda.idea.model;

import javax.xml.bind.annotation.XmlElement;

/**
 * Input POJO of the GetIdeas lambda
 */
public class GetIdeasRequest {
    
    private String cognitoId;
    private String email;
    private Boolean queryAll;
    
    @XmlElement
    public String getCognitoId() {
        return cognitoId;
    }
    public void setCognitoId(String cognitoId) {
        this.cognitoId = cognitoId;
    }
    
    @XmlElement
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    
    @XmlElement
    public Boolean getQueryAll() {
        return queryAll;
    }
    public void setQueryAll(Boolean queryAll) {
        this.queryAll = queryAll;
    }
}
