package de.vispron.idea.lambda.workingarea;

import java.util.List;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import de.vispron.idea.lambda.idea.model.Response;
import de.vispron.idea.lambda.idea.model.WorkingArea;

/**
 * Returns a list of working areas.
 * Deprecated -- no need for this functionality.
 */
@Deprecated
public class GetWorkingAreas implements RequestHandler<Object, Response> {

    private DynamoDBMapper mapper = new DynamoDBMapper(
            AmazonDynamoDBClientBuilder.standard().withRegion(Regions.EU_CENTRAL_1).build());

// out commented functionality to serverless.template
//    "GetWorkingAreas": {
//        "Type": "AWS::Serverless::Function",
//        "Properties": {
//            "FunctionName": "GetWorkinAreaHandler",
//            "Handler": "de.vispron.idea.lambda.workingarea.GetWorkingAreas",
//            "Runtime": "java8",
//            "CodeUri": "./target/idea-1.0.0.jar",
//            "Policies": [
//                "AmazonDynamoDBReadOnlyAccess",
//                "AWSLambdaFullAccess"
//            ]
//        }
//    },
//    "GetWorkingAreasPermissionToApi": {
//        "Type": "AWS::Lambda::Permission",
//        "Properties": {
//            "FunctionName": {
//                "Fn::Join": [
//                    "",
//                    [
//                        "arn:aws:lambda:",
//                        {
//                            "Ref": "AWS::Region"
//                        },
//                        ":",
//                        {
//                            "Ref": "AWS::AccountId"
//                        },
//                        ":function:",
//                        {
//                            "Ref": "GetWorkingAreas"
//                        }
//                    ]
//                ]
//            },
//            "Action": "lambda:InvokeFunction",
//            "Principal": "apigateway.amazonaws.com",
//            "SourceArn": {
//                "Fn::Join": [
//                    "",
//                    [
//                        "arn:aws:execute-api:",
//                        {
//                            "Ref": "AWS::Region"
//                        },
//                        ":",
//                        {
//                            "Ref": "AWS::AccountId"
//                        },
//                        ":",
//                        {
//                            "Ref": "IdeasApi"
//                        },
//                        "/*/GET/workingarea"
//                    ]
//                ]
//            }
//        }
//    },
//    "/workingarea": {
//        "get": {
//            "consumes": [
//                "application/json"
//            ],
//            "produces": [
//                "application/json"
//            ],
//            "responses": {
//                "200": {
//                    "description": "200 response",
//                    "schema": {
//                        "$ref": "#/definitions/WorkingAreaList"
//                    }
//                },
//                "400": {
//                    "description": "400 response"
//                },
//                "500": {
//                    "description": "500 response"
//                }
//            },
//            "security": [
//                {
//                    "IdeaCognitoAuthorizer": [
//                    ]
//                }
//            ],
//            "x-amazon-apigateway-integration": {
//                "uri": {
//                    "Fn::Join": [
//                        "",
//                        [
//                            "arn:aws:apigateway:eu-central-1:lambda:path/2015-03-31/functions/arn:aws:lambda:",
//                            {
//                                "Ref": "AWS::Region"
//                            },
//                            ":",
//                            {
//                                "Ref": "AWS::AccountId"
//                            },
//                            ":function:",
//                            {
//                                "Ref": "GetWorkingAreas"
//                            },
//                            "/invocations"
//                        ]
//                    ]
//                },
//                "responses": {
//                    "default": {
//                        "statusCode": "200"
//                    }
//                },
//                "passthroughBehavior": "when_no_match",
//                "httpMethod": "POST",
//                "contentHandling": "CONVERT_TO_TEXT",
//                "type": "aws"
//            }
//        }
//    },
//    "WorkingAreaList": {
//        "type": "array",
//        "items": {
//            "type": "string"
//        },
//    }"WorkingAreas": {
//        "Type": "AWS::DynamoDB::Table",
//        "Properties": {
//            "TableName": "WorkingAreas",
//            "AttributeDefinitions": [
//                {
//                    "AttributeName": "workingArea",
//                    "AttributeType": "S"
//                }
//            ],
//            "KeySchema": [
//                {
//                    "AttributeName": "workingArea",
//                    "KeyType": "HASH"
//                }
//            ],
//            "ProvisionedThroughput": {
//                "ReadCapacityUnits": 3,
//                "WriteCapacityUnits": 3
//            }
//        }
//    },


    
    /*
     * (non-Javadoc)
     * 
     * @see
     * com.amazonaws.services.lambda.runtime.RequestHandler#handleRequest(java.lang.
     * Object, com.amazonaws.services.lambda.runtime.Context)
     */
    @Override
    public Response handleRequest(Object input, Context context) {
        context.getLogger().log("Get workingareas");
        try {
            List<WorkingArea> workingAreas = mapper.scan(WorkingArea.class, new DynamoDBScanExpression());
            context.getLogger().log("working areas found : " + workingAreas.size());
            return new Response(200, workingAreas);
        } catch (Exception e) {
            return new Response(400, e);
        }
    }

}
