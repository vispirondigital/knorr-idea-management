package de.vispron.idea.lambda.auth;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.amazonaws.util.StringUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.vispron.idea.lambda.idea.model.Employee;

/**
 * Checks if registering user is in employee list. Compares email and
 * personalId.
 * 
 *
 */
public class PreSignUpTrigger implements RequestStreamHandler {

    // Jackson JSON mapper
    private ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public final static String REQUEST = "request";
    public final static String USER_ATTRIBUTES = "userAttributes";

    public final static String EMAIL = "email";
    public final static String CUSTOM_PERSONAL = "custom:personalId";

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.amazonaws.services.lambda.runtime.RequestStreamHandler#handleRequest(java
     * .io.InputStream, java.io.OutputStream,
     * com.amazonaws.services.lambda.runtime.Context)
     */
    @Override
    public void handleRequest(InputStream is, OutputStream os, Context context) throws IOException {
        JsonNode eventNode = OBJECT_MAPPER.readTree(is);
        JsonNode requestNode = eventNode.path(REQUEST);
        JsonNode userAttrsNode = requestNode.path(USER_ATTRIBUTES);

        String email = userAttrsNode.path(EMAIL).asText();
        String personalId = userAttrsNode.path(CUSTOM_PERSONAL).asText();

        if (StringUtils.isNullOrEmpty(email) || StringUtils.isNullOrEmpty(personalId)) {
            throw new RuntimeException("Wrong input format");
        }

        Employee user = Employee.get(personalId);

        if (user == null || !email.equals(user.getEmail()) || !user.getIsUser()) {
            throw new RuntimeException("Unauthorized - registered attributes not match");
        }
        context.getLogger().log("PreSignUp OK : " + email + " : " + personalId);
        OBJECT_MAPPER.writeValue(os, eventNode);
    }
}
