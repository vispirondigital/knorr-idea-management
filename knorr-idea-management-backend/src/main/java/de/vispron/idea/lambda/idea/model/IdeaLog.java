package de.vispron.idea.lambda.idea.model;

import java.util.*;
import java.util.Map.Entry;

import javax.xml.bind.annotation.XmlElement;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.MapDifference.ValueDifference;
import com.google.common.collect.Maps;

@DynamoDBDocument
public class IdeaLog {
    /**
     * A List of fields to ignore when calculating changes
     */
    private static final List<String> IGNORE_FIELDS = Arrays.asList("logs", "createdEmailBy");
    
    @DynamoDBIgnore
    private final ObjectMapper mapper = new ObjectMapper();

    public IdeaLog() {
    }

    public IdeaLog(IdeaDetail oldValue, IdeaDetail newValue, Employee employee) {
        this.changeAt = new Date();
        this.changedBy = employee;

        this.changelog = new HashMap<>();

        Map<String, Object> oldMap = getLogMap(oldValue);
        Map<String, Object> newMap = getLogMap(newValue);

        for (Entry<String, ValueDifference<Object>> entryDiff : Maps.difference(oldMap, newMap).entriesDiffering()
                .entrySet()) {

            this.changelog.put(entryDiff.getKey(),
                    new ChangeLogEntry(
                            entryDiff.getValue() != null && entryDiff.getValue().leftValue() != null
                                    ? entryDiff.getValue().leftValue().toString()
                                    : "",
                            entryDiff.getValue() != null && entryDiff.getValue().rightValue() != null
                                    ? entryDiff.getValue().rightValue().toString()
                                    : ""));
        }

    }

    /**
     * DynamoDB stores date as String in format ISO 8601 2018-10-16
     * 2018-10-16T07:44:05+00:00 2018-10-16T07:44:05Z 20181016T074405Z
     * 
     */
    private Date changeAt;

    private IdeaUser changedBy;

    private Map<String, ChangeLogEntry> changelog;

    @XmlElement
    @DynamoDBAttribute(attributeName = "changedAt")
    public Date getChangedAt() {
        return changeAt;
    }

    public void setChangedAt(Date createdAt) {
        this.changeAt = createdAt;
    }

    @XmlElement
    @DynamoDBAttribute(attributeName = "changedBy")
    public IdeaUser getChangedBy() {
        return changedBy;
    }

    public void setChangedBy(IdeaUser createdBy) {
        this.changedBy = createdBy;
    }

    @XmlElement
    @DynamoDBAttribute(attributeName = "changelog")
    public Map<String, ChangeLogEntry> getChangelog() {
        return changelog;
    }

    public void setChangelog(Map<String, ChangeLogEntry> changeLog) {
        this.changelog = changeLog;
    }
    
    /**
     * Convert the idea POJO into the MAP representation that should be used to calculate the changelog
     * @param value
     * @return
     */
    protected Map<String, Object> getLogMap(IdeaDetail value) {
        Map<String, Object> map = mapper.convertValue(value, Map.class);
        IGNORE_FIELDS.forEach(map::remove);
        return map;
    }

}
