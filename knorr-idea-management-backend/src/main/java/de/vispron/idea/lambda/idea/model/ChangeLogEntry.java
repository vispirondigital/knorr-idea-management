package de.vispron.idea.lambda.idea.model;

import javax.xml.bind.annotation.XmlElement;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;

/**
 * Provides oldValue and newValue
 *
 */
@DynamoDBDocument
public class ChangeLogEntry {

    public ChangeLogEntry() {
    }

    public ChangeLogEntry(String oldValue, String newValue) {
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    private String oldValue;
    private String newValue;

    @XmlElement
    @DynamoDBAttribute(attributeName = "old_value")
    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    @XmlElement
    @DynamoDBAttribute(attributeName = "new_value")
    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }
}