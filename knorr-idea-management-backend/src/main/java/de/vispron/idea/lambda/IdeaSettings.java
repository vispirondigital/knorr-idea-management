package de.vispron.idea.lambda;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;

public class IdeaSettings {
    public static String getRegion() {
        String region = System.getenv("REGION");
        if (region == null) {
            return Region.getRegion(Regions.EU_CENTRAL_1).getName();
        }
        return region;
    }
}
