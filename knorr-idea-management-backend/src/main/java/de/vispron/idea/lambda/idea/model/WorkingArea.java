package de.vispron.idea.lambda.idea.model;

import javax.xml.bind.annotation.XmlElement;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

/**
 * Entity for workingAreas table.
 * Deprecated -- no need for this functionality.
 */
@Deprecated
@DynamoDBTable(tableName = "WorkingAreas")
public class WorkingArea {

    public WorkingArea() {
    }

    private String workingArea;

    @XmlElement
    @DynamoDBHashKey(attributeName = "workingArea")
    public String getWorkingArea() {
        return workingArea;
    }

    public void setWorkingArea(String workingArea) {
        this.workingArea = workingArea;
    }

}
