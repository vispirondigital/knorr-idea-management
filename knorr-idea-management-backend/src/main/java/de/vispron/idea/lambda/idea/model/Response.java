package de.vispron.idea.lambda.idea.model;

import javax.xml.bind.annotation.XmlElement;

/**
 * Entity for json response, { statusCode: 200, body: {json} }
 *
 */
public class Response {

    public Response(Integer statusCode, Object body) {
        this.statusCode = statusCode;
        this.body = body;
    }

    private Integer statusCode;

    private Object body;

    @XmlElement
    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    @XmlElement
    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }
}
