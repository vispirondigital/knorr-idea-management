package de.vispron.idea.lambda.idea.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.fasterxml.jackson.annotation.JsonIgnore;

import de.vispron.idea.lambda.IdeaSettings;

/**
 * Element for employees list, Extended all from IdeaUser, Annotations are set
 * for UsersForApplication table.
 *
 */
@DynamoDBTable(tableName = "UsersForApplication")
public class Employee extends IdeaUser {

    @JsonIgnore
    private Boolean isUser;

    @JsonIgnore
    private Boolean isAdmin;

    private static final String TABLE_NAME = System.getenv("USERS_TABLE_NAME");

    private static DynamoDBMapper mapper = new DynamoDBMapper(
            AmazonDynamoDBClientBuilder.standard().withRegion(IdeaSettings.getRegion()).build());

    public static List<Employee> getList(DynamoDBScanExpression dynamoDBScanExpression) {
        DynamoDBMapperConfig configs = new DynamoDBMapperConfig.Builder()
                .withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(TABLE_NAME))
                .build();
        return mapper.scan(Employee.class, dynamoDBScanExpression, configs);
    }

    public static Employee get(String personalId) {
        DynamoDBMapperConfig configs = new DynamoDBMapperConfig.Builder()
                .withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(TABLE_NAME))
                .build();
        return mapper.load(Employee.class, personalId, configs);
    }

    public static void save(Employee Employee) {
        DynamoDBMapperConfig configs = new DynamoDBMapperConfig.Builder()
                .withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(TABLE_NAME))
                .build();
        mapper.save(Employee, configs);
    }

    @Override
    @XmlElement
    @DynamoDBHashKey(attributeName = "personalId")
    public String getPersonalId() {
        return super.getPersonalId();
    }

    @DynamoDBAttribute(attributeName = "isUser")
    public Boolean getIsUser() {
        return isUser;
    }

    public void setIsUser(Boolean isUser) {
        this.isUser = isUser;
    }

    @DynamoDBAttribute(attributeName = "isAdmin")
    public Boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

}
