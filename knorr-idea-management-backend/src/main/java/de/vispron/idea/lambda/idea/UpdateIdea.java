package de.vispron.idea.lambda.idea;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import de.vispron.idea.lambda.idea.model.Employee;
import de.vispron.idea.lambda.idea.model.IdeaDetail;
import de.vispron.idea.lambda.idea.model.IdeaLog;
import de.vispron.idea.lambda.idea.model.Response;

/**
 * Provides idea creation service.
 */
public class UpdateIdea implements RequestHandler<IdeaDetail, Response> {

    public static final String NO_CREATED_BY_COGNITO = "No createCognitoId for idea has been set.";

    public static final String NO_IDEA_STORED = "Not found idea";

    public static final String NO_AUTHORIZED = "Not authorized.";

    @Override
    public Response handleRequest(IdeaDetail update, Context context) {
        try {
            context.getLogger().log("Update idea " + update.getName() + " by " + update.getCreatedEmailBy());

            // load from db
            IdeaDetail load = IdeaDetail.loadIdea(update.getCreatedCognitoIdBy(), update.getCreatedAt());

            if (load == null) {
                return new Response(400, NO_IDEA_STORED);
            }

            Employee employee = getAdmin(update, context);

            if (employee == null || employee.getIsAdmin() == null || !employee.getIsAdmin()) {
                return new Response(400, NO_AUTHORIZED);
            }

            update.setCreatedEmailBy(load.getCreatedEmailBy());

            IdeaLog ideaLog = new IdeaLog(load, update, employee);
            if (load.getLogs() == null) {
                load.setLogs(new ArrayList<IdeaLog>());
            }
            update.setLogs(load.getLogs());
            update.getLogs().add(ideaLog);
            context.getLogger().log(load.toString());

            IdeaDetail.save(update);

            context.getLogger().log("Updated idea.");

            return new Response(200, "ok");
        } catch (Throwable e) {
            return new Response(400, e);
        }
    }

    /**
     * Returns authenticated user.
     * 
     * @param user
     * @param context
     * @return
     */
    private Employee getAdmin(IdeaDetail update, Context context) {

        Map<String, AttributeValue> map = new HashMap<String, AttributeValue>();
        map.put(":email", new AttributeValue(update.getCreatedEmailBy()));

        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression().withFilterExpression("email = :email")
                .withExpressionAttributeValues(map);

        List<Employee> scanResult = Employee.getList(scanExpression);

        context.getLogger().log(scanResult.toString());
        return scanResult.isEmpty() ? null : scanResult.get(0);
    }
}