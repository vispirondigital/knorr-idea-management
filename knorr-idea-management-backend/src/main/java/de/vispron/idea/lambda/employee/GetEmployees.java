package de.vispron.idea.lambda.employee;

import java.util.List;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.TableNameOverride;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import de.vispron.idea.lambda.idea.model.Employee;
import de.vispron.idea.lambda.idea.model.Response;

/**
 * Service returns list of employees
 *
 */
public class GetEmployees implements RequestHandler<Object, Response> {

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.amazonaws.services.lambda.runtime.RequestHandler#handleRequest(java.lang.
     * Object, com.amazonaws.services.lambda.runtime.Context)
     */
    @Override
    public Response handleRequest(Object object, Context context) {
        context.getLogger().log("Get Employees");
        try {
            List<Employee> employeesList = Employee.getList(new DynamoDBScanExpression());
            context.getLogger().log("Employees found : " + employeesList.size());
            return new Response(200, employeesList);
            
        } catch (Exception e) {
            return new Response(400, e);
        }
    }

}
