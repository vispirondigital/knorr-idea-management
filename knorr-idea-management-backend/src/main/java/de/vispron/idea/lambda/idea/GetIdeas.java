package de.vispron.idea.lambda.idea;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import de.vispron.idea.lambda.idea.model.GetIdeasRequest;
import de.vispron.idea.lambda.idea.model.Employee;
import de.vispron.idea.lambda.idea.model.IdeaDetail;
import de.vispron.idea.lambda.idea.model.Response;

/**
 * Returns the list of created ideas by cognitoId of owner.
 *
 */
public class GetIdeas implements RequestHandler<GetIdeasRequest, Response> {

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.amazonaws.services.lambda.runtime.RequestHandler#handleRequest(java.lang.
     * Object, com.amazonaws.services.lambda.runtime.Context)
     */
    @Override
    public Response handleRequest(GetIdeasRequest request, Context context) {

        try {
            context.getLogger().log("getIdeas for " + request.getCognitoId());

            Boolean isGetAll = getAllForAdminCheck(request, context);

            DynamoDBScanExpression scanExpression;
            if (isGetAll) {
                // if all items no need to set additional params
                scanExpression = new DynamoDBScanExpression();
            } else {
                Map<String, AttributeValue> map = new HashMap<String, AttributeValue>();
                map.put(":createdCognitoIdBy", new AttributeValue(request.getCognitoId()));
                scanExpression = new DynamoDBScanExpression()
                        .withFilterExpression("createdCognitoIdBy = :createdCognitoIdBy")
                        .withExpressionAttributeValues(map);
            }

            List<IdeaDetail> scanResult = IdeaDetail.getList(scanExpression);

            return new Response(200, scanResult);

        } catch (Exception e) {
            return new Response(400, e);
        }
    }

    /**
     * Checks and verifies the get all query parameter.
     * Verifies if the authenticated user has isAdmin flag.  
     * @param request
     * @param context
     * @return
     */
    private Boolean getAllForAdminCheck(GetIdeasRequest request, Context context) {

        if (request.getQueryAll() == null || !request.getQueryAll()) {
            return false;
        }

        Map<String, AttributeValue> map = new HashMap<String, AttributeValue>();
        map.put(":email", new AttributeValue(request.getEmail()));

        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression().withFilterExpression("email = :email")
                .withExpressionAttributeValues(map);

        List<Employee> scanResult = Employee.getList(scanExpression);

        if (scanResult.size() != 1) {
            context.getLogger().log("Found more employees for email");
            return false;
        }

        context.getLogger().log(request.getEmail() + " is admin : " + scanResult.get(0).getIsAdmin());
        return scanResult.get(0).getIsAdmin();
    }
}
