package de.vispron.idea.lambda.auth;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.vispron.idea.lambda.idea.model.Employee;

/**
 * Test for PreSignUpTrigger
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Employee.class })
@PowerMockIgnore({ "javax.management.*", "javax.net.ssl.*", "javax.security.*", "sun.security.*",
        "org.apache.http.conn.ssl.*", "com.amazonaws.http.conn.ssl.*" })
public class PreSignUpTriggerTest {

    @Mock
    private ObjectMapper objectMapper;

    @Mock
    private InputStream is;

    @Mock
    private OutputStream os;

    @Mock
    private Context context;

    @Mock
    private LambdaLogger logger;

    @Mock
    private JsonNode jsonNode;

    @Mock
    private JsonNode emailNode;

    @Mock
    private JsonNode personalNode;

    @Mock
    private JsonNode isUserNode;

    @Mock
    private Employee user;

    @InjectMocks
    private PreSignUpTrigger trigger;

    private static String EMAIL = "email@email.de";
    private static String PERSONAL_ID = "0123456789";
    private static String IS_USER = "isUser";

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Set up the mocks for test by the parameters of this method.
     * 
     * @param userAttributes
     * @param emailNode
     * @param personalNode
     * @param email
     * @param personalId
     * @param user
     * @throws JsonProcessingException
     * @throws IOException
     */
    private void setUpMocks(JsonNode userAttributes, JsonNode emailNode, JsonNode personalNode, String email,
            String personalId, Employee user, JsonNode isUserNode, Boolean isUser, String emailOfEmployee)
            throws JsonProcessingException, IOException {
        when(context.getLogger()).thenReturn(logger);
        when(objectMapper.readTree(is)).thenReturn(jsonNode);
        when(jsonNode.path(PreSignUpTrigger.REQUEST)).thenReturn(jsonNode);
        when(jsonNode.path(PreSignUpTrigger.USER_ATTRIBUTES)).thenReturn(userAttributes);

        when(jsonNode.path(PreSignUpTrigger.EMAIL)).thenReturn(emailNode);
        when(jsonNode.path(PreSignUpTrigger.CUSTOM_PERSONAL)).thenReturn(personalNode);
        when(jsonNode.path(IS_USER)).thenReturn(isUserNode);

        when(this.emailNode.asText()).thenReturn(email);
        when(this.personalNode.asText()).thenReturn(personalId);
        when(this.isUserNode.asBoolean()).thenReturn(isUser);

        PowerMockito.mockStatic(Employee.class);
        PowerMockito.when(Employee.get(any())).thenReturn(user);
        if (user != null) {
            when(user.getEmail()).thenReturn(emailOfEmployee);
            when(user.getIsUser()).thenReturn(isUser);
        }
    }

    /**
     * Checks if the number of mocks calling match the parameters of this method.
     * 
     * @param objectMapperReadCount
     * @param jsonReadRequestCount
     * @param jsonReadUserAttrCount
     * @param jsonReadEmailCount
     * @param jsonReadPersonalIdCount
     * @param emailNodoAsTextCount
     * @param personalNodeAsTextCount
     * @param dynamoGetItemCount
     * @param objectMapperWriteCount
     * @throws JsonProcessingException
     * @throws IOException
     */
    private void verifyMocksCallCounts(int objectMapperReadCount, int jsonReadRequestCount, int jsonReadUserAttrCount,
            int jsonReadEmailCount, int jsonReadPersonalIdCount, int emailNodoAsTextCount, int personalNodeAsTextCount,
            int dynamoGetItemCount, int objectMapperWriteCount) throws JsonProcessingException, IOException {

        verify(objectMapper, times(objectMapperReadCount)).readTree(is);
        verify(jsonNode, times(jsonReadRequestCount)).path(PreSignUpTrigger.REQUEST);
        verify(jsonNode, times(jsonReadUserAttrCount)).path(PreSignUpTrigger.USER_ATTRIBUTES);

        verify(jsonNode, times(jsonReadEmailCount)).path(PreSignUpTrigger.EMAIL);
        verify(jsonNode, times(jsonReadPersonalIdCount)).path(PreSignUpTrigger.CUSTOM_PERSONAL);

        verify(emailNode, times(emailNodoAsTextCount)).asText();
        verify(personalNode, times(personalNodeAsTextCount)).asText();

        PowerMockito.verifyStatic(Employee.class, times(dynamoGetItemCount));
        Employee.get(any());

        verify(objectMapper, times(objectMapperWriteCount)).writeValue(os, jsonNode);
        verify(context, times(objectMapperWriteCount)).getLogger();
    }

    /**
     * Simple test no properties, expected JsonMappingException
     */
    @Test
    public void preSignUpWithEmptyPropertiesTest() {
        try {
            (new PreSignUpTrigger()).handleRequest(null, null, null);
            fail("No error occured");
        } catch (IOException e) {
            assertEquals("com.fasterxml.jackson.databind.JsonMappingException", e.getClass().getName());
        }
    }

    /**
     * Happy scenario test
     * 
     * @throws JsonProcessingException
     * @throws IOException
     */
    @Test
    public void preSignUpEmailPersonalIdSetTest() throws JsonProcessingException, IOException {
        Map<String, AttributeValue> attMap = new HashMap<String, AttributeValue>();
        AttributeValue emailAttValue = new AttributeValue(EMAIL);
        AttributeValue isUserAttValue = new AttributeValue();
        isUserAttValue.setBOOL(true);
        attMap.put(PreSignUpTrigger.EMAIL, emailAttValue);
        attMap.put(IS_USER, isUserAttValue);

        setUpMocks(jsonNode, emailNode, personalNode, EMAIL, PERSONAL_ID, user, isUserNode, true, EMAIL);

        trigger.handleRequest(is, os, context);

        verifyMocksCallCounts(1, 1, 1, 1, 1, 1, 1, 1, 1);
    }

    /**
     * Not found record in dynamoDB for personalID Expected result
     * RuntimeException("Unauthorized")
     * 
     * @throws JsonProcessingException
     * @throws IOException
     */
    @Test
    public void preSignUpNoRecordFoundTest() throws JsonProcessingException, IOException {
        setUpMocks(jsonNode, emailNode, personalNode, EMAIL, PERSONAL_ID, null, isUserNode, true, EMAIL);
        // getItem return empty attributes mas
        // no record found in dynamoDB

        try {
            trigger.handleRequest(is, os, context);
            fail("No RuntimeException exception occured!");
        } catch (Exception e) {
            assertEquals("Unauthorized - registered attributes not match", e.getMessage());
        }

        verifyMocksCallCounts(1, 1, 1, 1, 1, 1, 1, 1, 0);
    }

    /**
     * Found record in dynamoDB for personalID has another email. Expected result
     * RuntimeException("Unauthorized")
     * 
     * @throws JsonProcessingException
     * @throws IOException
     */
    @Test
    public void preSignUpRecordFoundWithWrongMailTest() throws JsonProcessingException, IOException {
        // there is different mail
        setUpMocks(jsonNode, emailNode, personalNode, EMAIL, PERSONAL_ID, user, isUserNode, true, "fail@email.de");

        try {
            trigger.handleRequest(is, os, context);
            fail("No RuntimeException exception occured!");
        } catch (Exception e) {
            assertEquals("Unauthorized - registered attributes not match", e.getMessage());
        }

        verifyMocksCallCounts(1, 1, 1, 1, 1, 1, 1, 1, 0);
    }

    /**
     * Input has no UserAttributes node Expected result
     * RuntimeException("Unauthorized")
     * 
     * @throws JsonProcessingException
     * @throws IOException
     */
    @Test
    public void preSignUpEmailNoUserAttributesTest() throws JsonProcessingException, IOException {
        // no user attributes
        setUpMocks(null, emailNode, personalNode, EMAIL, PERSONAL_ID, user, isUserNode, true, null);

        try {
            trigger.handleRequest(is, os, context);
            fail("No RuntimeException exception occured!");
        } catch (Exception e) {
            assertEquals("java.lang.NullPointerException", e.getClass().getName());
        }

        verifyMocksCallCounts(1, 1, 1, 0, 0, 0, 0, 0, 0);
    }

    /**
     * Input has no email, personalId node Expected result
     * RuntimeException("Unauthorized")
     * 
     * @throws JsonProcessingException
     * @throws IOException
     */
    @Test
    public void preSignUpEmailNoEmailPersonalIdTest() throws JsonProcessingException, IOException {
        // no email
        setUpMocks(jsonNode, null, null, EMAIL, PERSONAL_ID, user, isUserNode, true, EMAIL);

        try {

            trigger.handleRequest(is, os, context);
            fail("No RuntimeException exception occured!");
        } catch (Exception e) {
            assertEquals("java.lang.NullPointerException", e.getClass().getName());
        }

        verifyMocksCallCounts(1, 1, 1, 1, 0, 0, 0, 0, 0);
    }

    /**
     * Input has empty email, personalId Expected result
     * RuntimeException("Unauthorized")
     * 
     * @throws JsonProcessingException
     * @throws IOException
     */
    @Test
    public void preSignUpEmailEmptyEmailPersonalIdTest() throws JsonProcessingException, IOException {
        // empty email, personalId
        setUpMocks(jsonNode, emailNode, personalNode, "", "", user, isUserNode, true, null);

        try {
            trigger.handleRequest(is, os, context);
            fail("No RuntimeException exception occured!");
        } catch (Exception e) {
            assertEquals("Wrong input format", e.getMessage());
        }

        verifyMocksCallCounts(1, 1, 1, 1, 1, 1, 1, 0, 0);
    }

    /**
     * Found record in dynamoDB for personalID has false isUser. Expected result
     * RuntimeException("Unauthorized")
     * 
     * @throws JsonProcessingException
     * @throws IOException
     */
    @Test
    public void preSignUpIsUserFalseTest() throws JsonProcessingException, IOException {
        setUpMocks(jsonNode, emailNode, personalNode, EMAIL, PERSONAL_ID, user, isUserNode, true, null);

        try {
            trigger.handleRequest(is, os, context);
            fail("No RuntimeException exception occured!");
        } catch (Exception e) {
            assertEquals("Unauthorized - registered attributes not match", e.getMessage());
        }

        verifyMocksCallCounts(1, 1, 1, 1, 1, 1, 1, 1, 0);
    }

}
