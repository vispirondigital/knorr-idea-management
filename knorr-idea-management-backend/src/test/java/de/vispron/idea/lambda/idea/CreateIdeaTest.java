package de.vispron.idea.lambda.idea;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;

import de.vispron.idea.lambda.idea.model.IdeaDetail;
import de.vispron.idea.lambda.idea.model.Response;

/**
 * Test for CreateIdea
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ IdeaDetail.class })
@PowerMockIgnore({ "javax.management.*", "javax.net.ssl.*", "javax.security.*", "sun.security.*",
        "org.apache.http.conn.ssl.*", "com.amazonaws.http.conn.ssl.*" })
public class CreateIdeaTest {

    @Mock
    private Context context;

    @Mock
    private LambdaLogger logger;

    @InjectMocks
    private CreateIdea idea;

    private final String NAME = "idea name";
    private final String DESCRIPTION = "description description";
    private final String CRETED_BY_EMAIL = "name@email.de";
    private final String CRETED_BY_COGNITO = "321654987";
    private final String ITEM_TYPE = "group";
    private final String CATEGORY = "categoryNo1";

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    private IdeaDetail setUpTest(String name, String description, String email, String cognitoId, String itemType,
            String category, List<String> workingArea) {

        IdeaDetail create = new IdeaDetail();
        create.setName(name);
        create.setDescription(description);
        create.setCreatedEmailBy(email);
        create.setCreatedCognitoIdBy(cognitoId);
        create.setIdeaType(itemType);
        if (category != null) {
            create.setCategory(new ArrayList<String>());
            create.getCategory().add(category);
        }

        create.setWorkingArea(workingArea);

        when(context.getLogger()).thenReturn(logger);

        PowerMockito.mockStatic(IdeaDetail.class);

        return create;
    }

    /**
     * Tests for create idea, expected 200 response code.
     * 
     */
    @Test
    public void cretedIdeaTest() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("areaNo1");

        IdeaDetail create = setUpTest(NAME, DESCRIPTION, CRETED_BY_EMAIL, CRETED_BY_COGNITO, ITEM_TYPE, CATEGORY, list);

        Response response = idea.handleRequest(create, context);

        assertEquals(new Integer(200), response.getStatusCode());
        assertTrue(response.getBody().toString().startsWith("ok"));
    }

    /**
     * Tests for create idea, missing name.
     * 
     */
    @Test
    public void cretedIdeaNoNameTest() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("areaNo1");

        IdeaDetail create = setUpTest("", DESCRIPTION, CRETED_BY_EMAIL, CRETED_BY_COGNITO, ITEM_TYPE, CATEGORY, list);

        Response response = idea.handleRequest(create, context);

        assertEquals(new Integer(400), response.getStatusCode());
        assertEquals(CreateIdea.NO_NAME, response.getBody());
    }

    /**
     * Tests for create idea, missing description.
     * 
     */
    @Test
    public void cretedIdeaNoDescriptionTest() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("areaNo1");

        IdeaDetail create = setUpTest(NAME, "", CRETED_BY_EMAIL, CRETED_BY_COGNITO, ITEM_TYPE, CATEGORY, list);

        Response response = idea.handleRequest(create, context);

        assertEquals(new Integer(400), response.getStatusCode());
        assertEquals(CreateIdea.NO_DESCRIPTION, response.getBody());
    }

    /**
     * Tests for create idea, missing email.
     * 
     */
    @Test
    public void cretedIdeaNoEmailTest() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("areaNo1");

        IdeaDetail create = setUpTest(NAME, DESCRIPTION, null, CRETED_BY_COGNITO, ITEM_TYPE, CATEGORY, list);

        Response response = idea.handleRequest(create, context);

        assertEquals(new Integer(400), response.getStatusCode());
        assertEquals(CreateIdea.NO_CREATED_BY, response.getBody());
    }

    /**
     * Tests for create idea, missing cognitoId.
     * 
     */
    @Test
    public void cretedIdeaNoCognitoIdTest() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("areaNo1");

        IdeaDetail create = setUpTest(NAME, DESCRIPTION, CRETED_BY_EMAIL, null, ITEM_TYPE, CATEGORY, list);

        Response response = idea.handleRequest(create, context);

        assertEquals(new Integer(400), response.getStatusCode());
        assertEquals(CreateIdea.NO_CREATED_BY_COGNITO, response.getBody());
    }

    /**
     * Tests for create idea, missing type.
     * 
     */
    @Test
    public void cretedIdeaNoIdeaTypeTest() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("areaNo1");

        IdeaDetail create = setUpTest(NAME, DESCRIPTION, CRETED_BY_EMAIL, CRETED_BY_COGNITO, "", CATEGORY, list);

        Response response = idea.handleRequest(create, context);

        assertEquals(new Integer(400), response.getStatusCode());
        assertEquals(CreateIdea.NO_TYPE, response.getBody());
    }

    /**
     * Tests for create idea, missing type.
     * 
     */
    @Test
    public void cretedIdeaNoCategoryTest() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("areaNo1");

        IdeaDetail create = setUpTest(NAME, DESCRIPTION, CRETED_BY_EMAIL, CRETED_BY_COGNITO, ITEM_TYPE, null, list);

        Response response = idea.handleRequest(create, context);

        assertEquals(new Integer(400), response.getStatusCode());
        assertEquals(CreateIdea.NO_CATEGORY, response.getBody());
    }

    /**
     * Tests for create idea, missing working area.
     * 
     */
    @Test
    public void cretedIdeaNoWorkingAreaTest() {
        ArrayList<String> list = new ArrayList<String>();

        IdeaDetail create = setUpTest(NAME, DESCRIPTION, CRETED_BY_EMAIL, CRETED_BY_COGNITO, ITEM_TYPE, CATEGORY, list);

        Response response = idea.handleRequest(create, context);

        assertEquals(new Integer(400), response.getStatusCode());
        assertEquals(CreateIdea.NO_WORKING, response.getBody());
    }
}