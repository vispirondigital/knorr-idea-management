package de.vispron.idea.lambda.employee;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;

import de.vispron.idea.lambda.idea.model.Employee;
import de.vispron.idea.lambda.idea.model.Response;

/**
 * Test for GetEmployees
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Employee.class })
@PowerMockIgnore({ "javax.management.*", "javax.net.ssl.*", "javax.security.*", "sun.security.*",
        "org.apache.http.conn.ssl.*", "com.amazonaws.http.conn.ssl.*" })
public class GetEmployeesTest {

    @Mock
    private Context context;

    @Mock
    private LambdaLogger logger;

    @Mock
    private List<Employee> list;

    @InjectMocks
    private GetEmployees employeeService;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    private String EMAIL_1 = "email1@email.de";
    private String FIRST_NAME_1 = "firstName1";
    private String LAST_NAME_1 = "firstName1";
    private String PERSONAL_ID_1 = "11223344556677889900";

    private String EMAIL_2 = "2email2@email.de";
    private String FIRST_NAME_2 = "2firstName2";
    private String LAST_NAME_2 = "2firstName2";
    private String PERSONAL_ID_2 = "222002";

    /**
     * Returns expected list and code.
     */
    @Test
    public void returnListTest() {
        when(context.getLogger()).thenReturn(logger);

        PowerMockito.mockStatic(Employee.class);
        PowerMockito.when(Employee.getList(any())).thenReturn(list);

        Employee employee1 = new Employee();
        employee1.setEmail(EMAIL_1);
        employee1.setFirstName(FIRST_NAME_1);
        employee1.setLastName(LAST_NAME_1);
        employee1.setPersonalId(PERSONAL_ID_1);

        Employee employee2 = new Employee();
        employee2.setEmail(EMAIL_2);
        employee2.setFirstName(FIRST_NAME_2);
        employee2.setLastName(LAST_NAME_2);
        employee2.setPersonalId(PERSONAL_ID_2);

        when(list.get(0)).thenReturn(employee1);
        when(list.get(1)).thenReturn(employee2);

        Response response = employeeService.handleRequest(null, context);

        assertEquals(new Integer(200), response.getStatusCode());
        assertEquals(list, response.getBody());

        assertEquals(EMAIL_1, ((List<Employee>) response.getBody()).get(0).getEmail());
        assertEquals(FIRST_NAME_1, ((List<Employee>) response.getBody()).get(0).getFirstName());
        assertEquals(LAST_NAME_1, ((List<Employee>) response.getBody()).get(0).getLastName());
        assertEquals(PERSONAL_ID_1, ((List<Employee>) response.getBody()).get(0).getPersonalId());

        assertEquals(EMAIL_2, ((List<Employee>) response.getBody()).get(1).getEmail());
        assertEquals(FIRST_NAME_2, ((List<Employee>) response.getBody()).get(1).getFirstName());
        assertEquals(LAST_NAME_2, ((List<Employee>) response.getBody()).get(1).getLastName());
        assertEquals(PERSONAL_ID_2, ((List<Employee>) response.getBody()).get(1).getPersonalId());

        PowerMockito.verifyStatic(Employee.class, times(1));
        Employee.getList(any());
    }
}
