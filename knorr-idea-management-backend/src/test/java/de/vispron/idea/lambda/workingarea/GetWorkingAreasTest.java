package de.vispron.idea.lambda.workingarea;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedScanList;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;

import de.vispron.idea.lambda.idea.model.Response;
import de.vispron.idea.lambda.idea.model.WorkingArea;

/**
 * Get working areas test. Deprecated -- no need for this functionality.
 */
@Deprecated
public class GetWorkingAreasTest {

    @Mock
    private Context context;

    @Mock
    private LambdaLogger logger;

    @Mock
    private DynamoDBMapper mapper;

    @Mock
    private PaginatedScanList<WorkingArea> list;

    @InjectMocks
    private GetWorkingAreas waService;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Returns expected list and code.
     */
    @Test
    public void returnListTest() {
        when(context.getLogger()).thenReturn(logger);
        when(mapper.scan(Matchers.any(Class.class), any())).thenReturn(list);

        Response response = waService.handleRequest(null, context);

        assertEquals(new Integer(200), response.getStatusCode());
        assertEquals(list, response.getBody());
    }

}
