package de.vispron.idea.lambda.idea;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.vispron.idea.lambda.idea.model.Employee;
import de.vispron.idea.lambda.idea.model.IdeaDetail;
import de.vispron.idea.lambda.idea.model.IdeaLog;
import de.vispron.idea.lambda.idea.model.Response;

/**
 * Test for UpdateIdea
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ IdeaDetail.class, Employee.class, IdeaLog.class, ObjectMapper.class })
@PowerMockIgnore({ "javax.management.*", "javax.net.ssl.*", "javax.security.*", "sun.security.*",
        "org.apache.http.conn.ssl.*", "com.amazonaws.http.conn.ssl.*" })
public class UpdateIdeaTest {

    @Mock
    private Context context;

    @Mock
    private LambdaLogger logger;

    @Mock
    private IdeaDetail loaded;

    @Mock
    private List<Employee> empList;

    @Mock
    private IdeaLog ideaLog;

    @Mock
    private Employee user;

    @Mock
    private ObjectMapper mapper;

    @InjectMocks
    private UpdateIdea idea;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(IdeaDetail.class);
        PowerMockito.mockStatic(Employee.class);
    }

    /**
     * Tests for create idea, not found object for update.
     * 
     * @throws Exception
     * 
     */
    @Test
    public void updateIdeaNothingToUpdateTest() throws Exception {
        when(context.getLogger()).thenReturn(logger);

        IdeaDetail ideaDeatail = new IdeaDetail();
        ideaDeatail.setCreatedCognitoIdBy("cognitoId");
        ideaDeatail.setCreatedAt(new Date());

        when(IdeaDetail.loadIdea(any(), any())).thenReturn(null);

        Response response = idea.handleRequest(ideaDeatail, context);

        assertEquals(new Integer(400), response.getStatusCode());
        assertEquals(UpdateIdea.NO_IDEA_STORED, response.getBody());

        PowerMockito.verifyStatic(IdeaDetail.class, times(1));
        IdeaDetail.loadIdea(any(), any());
        PowerMockito.verifyStatic(IdeaDetail.class, never());
        IdeaDetail.save(any());
    }

    /**
     * Tests for create idea, success.
     * 
     * @throws Exception
     * 
     */
    @Test
    public void updateIdeaNUpdateTest() throws Exception {
        when(context.getLogger()).thenReturn(logger);

        IdeaDetail ideaDeatail = new IdeaDetail();
        ideaDeatail.setCreatedCognitoIdBy("cognitoId");
        ideaDeatail.setCreatedAt(new Date());

        when(IdeaDetail.loadIdea(any(), any())).thenReturn(loaded);
        when(Employee.getList(any())).thenReturn(empList);
        when(empList.get(0)).thenReturn(user);
        when(user.getIsAdmin()).thenReturn(true);

        PowerMockito.whenNew(ObjectMapper.class).withNoArguments().thenReturn(mapper);

        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        when(mapper.convertValue(any(IdeaDetail.class), eq(Map.class))).thenReturn(hashMap);
        PowerMockito.whenNew(IdeaLog.class).withAnyArguments().thenReturn(ideaLog);

        Response response = idea.handleRequest(ideaDeatail, context);

        assertEquals(new Integer(200), response.getStatusCode());
        assertEquals("ok", response.getBody());

        PowerMockito.verifyStatic(IdeaDetail.class, times(1));
        IdeaDetail.loadIdea(any(), any());
        PowerMockito.verifyStatic(IdeaDetail.class, times(1));
        IdeaDetail.save(any());

    }

    /**
     * Tests for create idea, no user found.
     * 
     * @throws Exception
     * 
     */
    @Test
    public void updateIdeaNoUserTest() throws Exception {
        when(context.getLogger()).thenReturn(logger);

        IdeaDetail ideaDeatail = new IdeaDetail();
        ideaDeatail.setCreatedCognitoIdBy("cognitoId");
        ideaDeatail.setCreatedAt(new Date());

        when(IdeaDetail.loadIdea(any(), any())).thenReturn(loaded);
        when(Employee.getList(any())).thenReturn(new ArrayList<Employee>());
        when(empList.get(0)).thenReturn(user);
        when(user.getIsAdmin()).thenReturn(true);

        Response response = idea.handleRequest(ideaDeatail, context);

        assertEquals(new Integer(400), response.getStatusCode());
        assertEquals(UpdateIdea.NO_AUTHORIZED, response.getBody());

        PowerMockito.verifyStatic(IdeaDetail.class, times(1));
        IdeaDetail.loadIdea(any(), any());
        PowerMockito.verifyStatic(IdeaDetail.class, never());
        IdeaDetail.save(any());

    }

    /**
     * Tests for create idea, no admin.
     * 
     * @throws Exception
     * 
     */
    @Test
    public void updateIdeaNoAdminTest() throws Exception {
        when(context.getLogger()).thenReturn(logger);

        IdeaDetail ideaDeatail = new IdeaDetail();
        ideaDeatail.setCreatedCognitoIdBy("cognitoId");
        ideaDeatail.setCreatedAt(new Date());

        when(IdeaDetail.loadIdea(any(), any())).thenReturn(loaded);
        when(Employee.getList(any())).thenReturn(empList);
        when(empList.get(0)).thenReturn(user);
        when(user.getIsAdmin()).thenReturn(false);

        Response response = idea.handleRequest(ideaDeatail, context);

        assertEquals(new Integer(400), response.getStatusCode());
        assertEquals(UpdateIdea.NO_AUTHORIZED, response.getBody());

        PowerMockito.verifyStatic(IdeaDetail.class, times(1));
        IdeaDetail.loadIdea(any(), any());
        PowerMockito.verifyStatic(IdeaDetail.class, never());
        IdeaDetail.save(any());

    }
}