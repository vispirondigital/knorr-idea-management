package de.vispron.idea.lambda.idea;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedScanList;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;

import de.vispron.idea.lambda.idea.model.GetIdeasRequest;
import de.vispron.idea.lambda.idea.model.Employee;
import de.vispron.idea.lambda.idea.model.IdeaDetail;
import de.vispron.idea.lambda.idea.model.IdeaUser;
import de.vispron.idea.lambda.idea.model.Response;

/**
 * Test for IdeasByEmailTest
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ IdeaDetail.class, Employee.class })
@PowerMockIgnore({ "javax.management.*", "javax.net.ssl.*", "javax.security.*", "sun.security.*",
        "org.apache.http.conn.ssl.*", "com.amazonaws.http.conn.ssl.*" })
public class GetIdeasTest {

    @Mock
    private Context context;

    @Mock
    private IdeaUser ideaUser;

    @Mock
    private List<IdeaDetail> list;

    @Mock
    private LambdaLogger logger;

    @Mock
    private Employee user;

    @Mock
    private GetIdeasRequest logedUser;

    @Mock
    private PaginatedScanList<Employee> userList;

    private String COGNITO_ID = "0123456789";
    private String EMAIL = "email@email.de";

    private String NAME_1 = "name1";
    private String NAME_2 = "name1";

    @InjectMocks
    private GetIdeas ideasService;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(IdeaDetail.class);
        PowerMockito.mockStatic(Employee.class);
    }

    /**
     * For input mail test returns expected list and code.
     */
    @Test
    public void simpleNameFoundTest() {
        Mockito.when(context.getLogger()).thenReturn(logger);

        PowerMockito.when(IdeaDetail.getList(any())).thenReturn(list);

        IdeaDetail ideaDetail1 = new IdeaDetail();
        ideaDetail1.setCreatedEmailBy(EMAIL);
        ideaDetail1.setCreatedCognitoIdBy(COGNITO_ID);
        ideaDetail1.setName(NAME_1);

        IdeaDetail ideaDetail2 = new IdeaDetail();
        ideaDetail2.setCreatedEmailBy(EMAIL);
        ideaDetail2.setCreatedCognitoIdBy(COGNITO_ID);
        ideaDetail2.setName(NAME_2);

        Mockito.when(list.get(0)).thenReturn(ideaDetail1);
        Mockito.when(list.get(1)).thenReturn(ideaDetail2);

        GetIdeasRequest logedUser = new GetIdeasRequest();
        logedUser.setCognitoId(COGNITO_ID);
        logedUser.setEmail(EMAIL);

        Response response = ideasService.handleRequest(logedUser, context);

        assertEquals(new Integer(200), response.getStatusCode());
        assertEquals(list, response.getBody());

        assertEquals(NAME_1, ((List<IdeaDetail>) response.getBody()).get(0).getName());
        assertEquals(EMAIL, ((List<IdeaDetail>) response.getBody()).get(0).getCreatedEmailBy());
        assertEquals(COGNITO_ID, ((List<IdeaDetail>) response.getBody()).get(0).getCreatedCognitoIdBy());

        assertEquals(NAME_2, ((List<IdeaDetail>) response.getBody()).get(1).getName());
        assertEquals(EMAIL, ((List<IdeaDetail>) response.getBody()).get(1).getCreatedEmailBy());
        assertEquals(COGNITO_ID, ((List<IdeaDetail>) response.getBody()).get(1).getCreatedCognitoIdBy());
        
        PowerMockito.verifyStatic(Employee.class, never());
        Employee.getList(any());
        
        PowerMockito.verifyStatic(IdeaDetail.class, times(1));
        IdeaDetail.getList(any());
    }

    /**
     * For input null mail test returns expected exception and 400 code.
     */
    @Test
    public void simpleNullInputTest() {
        Mockito.when(context.getLogger()).thenReturn(logger);
        Mockito.when(context.getIdentity()).thenReturn(null);
        Response response = ideasService.handleRequest(null, context);

        assertEquals(new Integer(400), response.getStatusCode());
        assertEquals(NullPointerException.class, response.getBody().getClass());
        
        PowerMockito.verifyStatic(Employee.class, never());
        Employee.getList(any());
        
        PowerMockito.verifyStatic(IdeaDetail.class, never());
        IdeaDetail.getList(any());
    }

    /**
     * Sets up variables for test with query all test
     * @param isAllQuery
     */
    private void updateTestForAllQuerry(Boolean isAllQuery) {
        when(context.getLogger()).thenReturn(logger);
        
        PowerMockito.when(IdeaDetail.getList(any(DynamoDBScanExpression.class))).thenReturn(list);

        IdeaDetail ideaDetail1 = new IdeaDetail();
        ideaDetail1.setCreatedEmailBy(EMAIL);
        ideaDetail1.setCreatedCognitoIdBy(COGNITO_ID);
        ideaDetail1.setName(NAME_1);

        IdeaDetail ideaDetail2 = new IdeaDetail();
        ideaDetail2.setCreatedEmailBy(EMAIL);
        ideaDetail2.setCreatedCognitoIdBy(COGNITO_ID);
        ideaDetail2.setName(NAME_2);

        when(list.get(0)).thenReturn(ideaDetail1);
        when(list.get(1)).thenReturn(ideaDetail2);

        when(logedUser.getCognitoId()).thenReturn(COGNITO_ID);
        when(logedUser.getEmail()).thenReturn(EMAIL);
        when(logedUser.getQueryAll()).thenReturn(isAllQuery);

        PowerMockito.when(Employee.getList(any(DynamoDBScanExpression.class))).thenReturn(userList);
        when(userList.size()).thenReturn(1);
        when(userList.get(0)).thenReturn(user);
    }

    /**
     * User is admin, expected once called cognito.getCognitoId()
     */
    @Test
    public void getIdeasQueryAllTrueIsAdmin() {
        updateTestForAllQuerry(true);
        when(user.getIsAdmin()).thenReturn(true);

        Response response = ideasService.handleRequest(logedUser, context);
        assertEquals(new Integer(200), response.getStatusCode());

        verify(logedUser, times(1)).getCognitoId();
        
        
        PowerMockito.verifyStatic(Employee.class, times(1));
        Employee.getList(any());
        
        PowerMockito.verifyStatic(IdeaDetail.class, times(1));
        IdeaDetail.getList(any());
    }

    /**
     * User is not admin, expected 2 times called cognito.getCognitoId() because
     * user is not admin, need extra call getCognitoId for getIdeas
     */
    @Test
    public void getIdeasQueryAllFalseIsAdmin() {
        updateTestForAllQuerry(false);
        when(user.getIsAdmin()).thenReturn(false);
        Response response = ideasService.handleRequest(logedUser, context);
        assertEquals(new Integer(200), response.getStatusCode());

        verify(logedUser, times(2)).getCognitoId();
        
        PowerMockito.verifyStatic(Employee.class, never());
        Employee.getList(any());
        
        PowerMockito.verifyStatic(IdeaDetail.class, times(1));
        IdeaDetail.getList(any());
    }

    /**
     * There is not call query all, expected 2 times called cognito.getCognitoId(),
     * need extra call getCognitoId for getIdeas
     */
    @Test
    public void getIdeasNoQueryAll() {
        updateTestForAllQuerry(null);
        when(user.getIsAdmin()).thenReturn(true);
        Response response = ideasService.handleRequest(logedUser, context);
        assertEquals(new Integer(200), response.getStatusCode());

        verify(logedUser, times(2)).getCognitoId();
        
        PowerMockito.verifyStatic(Employee.class, never());
        Employee.getList(any());
        
        PowerMockito.verifyStatic(IdeaDetail.class, times(1));
        IdeaDetail.getList(any());
    }
}
