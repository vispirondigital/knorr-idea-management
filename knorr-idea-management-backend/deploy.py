#!/usr/bin/env python3
import argparse
import getpass
import platform
import uuid

from deploy_helper.dict_action import DictAction
from deploy_helper.env_choices import EnvChoices
from deploy_helper.generic import getenv, get_commit_hash, query_yes_no
from deploy_helper.runner import run_command
from deploy_helper.s3_commands import get_user_pool, get_rest_api, get_existing_user_pools, ensure_client, \
    get_user_pool_client_secret, get_cognito_domain, get_id_from_run_create_deployment, convert_template, \
    create_move_to_s3_file_command, \
    create_tag_s3_file_command


# ----------------------------------------------------------------------------------------------------------------------
arg_parser = argparse.ArgumentParser(description='Deploy process to the cloud')

envCompany = getenv("CLOUD_COMPANY")
arg_parser.add_argument('--company', type=str, help='default is $CLOUD_COMPANY',
                        required=envCompany is None, default=envCompany, metavar='"Company GmbH"'
                        )

envApp = getenv("CLOUD_APPLICATION")
arg_parser.add_argument('--application', type=str, help='default is $CLOUD_APPLICATION',
                        required=envApp is None, default=envApp, metavar='"App Name"'
                        )

arg_parser.add_argument('--environment', type=str, help='default is DEV-user@host', choices=EnvChoices())
arg_parser.add_argument('--aws-region', type=str, help='AWS Region for deployment', default="eu-central-1")
arg_parser.add_argument('--appVersion', type=str, default="SNAPSHOT", metavar="x.y.z")
arg_parser.add_argument('--deployedBy', type=str, metavar="user@host",
                        default=getpass.getuser() + "-" + platform.node())

envBucket = getenv("CLOUD_S3_BUCKET") or "knorr-idea-serverless-bucket"
arg_parser.add_argument('--s3-bucket', type=str,
                        help="The S3 bucket used for storage of deploy artifacts. Default is $CLOUD_S3_BUCKET",
                        metavar="S3 Bucket", default=envBucket)

arg_parser.add_argument('-t', '--tag', action=DictAction, metavar="KEY=value")
arg_parser.add_argument('--dry-run', help="Print commands but do NOT execute", action="store_true")
arg_parser.add_argument('--skip-tests', help="Skip maven tests", action="store_true")

args = arg_parser.parse_args()

# Constants    
args.uuid = uuid.uuid4().urn[9:]
args.commitHash = get_commit_hash()

if args.environment is None:
    args.environment = ("DEV-" + args.deployedBy).replace(".", "-")

print("-----")
d = vars(args)
for key in d:
    print(key + ": " + repr(d[key]))

do_deploy = query_yes_no("----\nShall we proceed with build & deployment?", default="no")
if not do_deploy:
    exit(0)
# ----------------------------------------------------------------------------------------------------------------------


# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------- START DEPLOYMENT HERE ------------------------------------------------------

build_command = ["mvn", "clean", "install",
                 "-Didea.deploy.version=%s" % args.appVersion,
                 "-DREGION=%s" % args.aws_region
                 ]
if args.skip_tests:
    build_command.append("-DskipTests=true")
run_command(build_command, args)

run_command(["aws", "cloudformation", "package",
             "--template-file", "serverless.template",
             "--output-template-file", "output-conf.template",
             "--s3-bucket", args.s3_bucket], args)

tags = {}
if args.tag is not None:
    tags.update(args.tag)
tags["company"] = args.company
tags["application"] = args.application
tags["deployedBy"] = args.deployedBy
tags["environment"] = args.environment
tags["version"] = args.appVersion
tags["commitHash"] = args.commitHash
tags["uniqueUUID"] = args.uuid

deploy_command = [
    "aws", "cloudformation", "deploy",
    "--template-file", "output-conf.template",
    "--stack-name", "stack-from-%s" % args.environment,
    "--capabilities", "CAPABILITY_IAM",

    "--parameter-overrides",
                    "environment=%s" % args.environment,
                    "version=%s" % args.appVersion,
                    "awsregion=%s" % args.aws_region,
                    "company=%s" % args.company,
                    "application=%s" % args.application,
                    "deployedBy=%s" % args.deployedBy,

    "--tags",
]
for key in tags:
    deploy_command.append("%s=%s" % (key, tags[key]))

run_command(deploy_command, args)

# ---------------------------- GET ALL CONFIGURATION INFORMATION ------------------------------------


rest_api_id = get_rest_api(args)['id']

# print(get_user_pool()['Id'])
stack_runtime_parameters = {
    "SERVICEURI": 'https://%s.execute-api.%s.amazonaws.com/%s' % (rest_api_id, args.aws_region, args.environment),
    "USERPOOLID": get_user_pool(args)['Id'],
    # "CLIENTID": ... will be filled in later
}


# ----------------------------------------------------------------------------------------------------------------------
# ------------------------------------ CONFIGURE COGNITO ------------------------------------------------------
existing_user_pools = get_existing_user_pools(args, stack_runtime_parameters["USERPOOLID"])
if not existing_user_pools:
    existing_user_pools = []

android_client_id = ensure_client(
    args, "android-client-%s" % args.environment, "USER_PASSWORD_AUTH", True,
    existing_user_pools, stack_runtime_parameters["USERPOOLID"])

web_client_id = ensure_client(
    args, "web-client-%s" % args.environment, "ADMIN_NO_SRP_AUTH", False,
    existing_user_pools, stack_runtime_parameters["USERPOOLID"])

stack_runtime_parameters["CLIENTID"] = web_client_id

android_client_secret = get_user_pool_client_secret(args, android_client_id, stack_runtime_parameters["USERPOOLID"])


# ----------------------------------------------------------------------------------------------------------------------
# ------------------------------------ CREATE COGNITO DOMAIN ------------------------------------------------------
user_pool_domain = get_cognito_domain(args, stack_runtime_parameters["USERPOOLID"])

if len(user_pool_domain) == 0:
    # Let's create a random pool name (AWS says its length <= 63
    user_pool_domain = ("%s-%s-user-poolidea" % (args.uuid, args.environment.lower()))[:63]

    run_command(["aws", "cognito-idp", "create-user-pool-domain",
                 "--domain", user_pool_domain,
                 "--user-pool-id", stack_runtime_parameters["USERPOOLID"]
                 ], args)

# ----------------------------------------------------------------------------------------------------------------------
# ------------------------------------ UPDATE DEPLOYMENT STAGE ---------------------------------------------------
update_stage = ["aws", "apigateway", "update-stage",
                "--stage-name", "%s" % args.environment,
                "--rest-api-id", "%s" % rest_api_id,
                "--patch-operations",
                "op=replace,path=/deploymentId,value=%s" % get_id_from_run_create_deployment(
                    args, rest_api_id,
                    args.environment)]

run_command(update_stage, args)

# ----------------------------------------------------------------------------------------------------------------------
# -------------------------------------- DEPLOY HTML ADMIN SITE --------------------------------------------------------

# Replace the placeholders in the HTML file with the RUNTIME correct values
convert_template(
    "../knorr-idea-management-web/get-idea-list.html",
    "target/get-idea-list.html",
    stack_runtime_parameters)
run_command(create_move_to_s3_file_command(args, "get-idea-list.html", basepath="target/"), args)

for file in ["js/amazon-cognito-identity.js", "js/aws-sdk-2.345.0.min.js"]:
    run_command(create_move_to_s3_file_command(args, file), args)

# Add tags to the uploaded files
for file in ["get-idea-list.html", "js/amazon-cognito-identity.js", "js/aws-sdk-2.345.0.min.js"]:
    run_command(create_tag_s3_file_command(args, file, tags), args)

# ----------------------------------------------------------------------------------------------------------------------
# --------- DEPLOYMENT IS FINISHED --> PRINT USEFUL DATA
print("\n------ DEPLOYMENT FINISHED --------")
print("ENVIRONMENT :          %s\n" % args.environment)
print("User Pool ID:          %s" % stack_runtime_parameters["USERPOOLID"])
print("Service URI:           %s" % stack_runtime_parameters["SERVICEURI"])
print("User Pool Domain:      %s" % user_pool_domain)
print("Android Client ID:     %s" % android_client_id)
print("Android Client SECRET: %s" % android_client_secret)
